const path = require('path');
const { override, addBabelPlugins, babelInclude } = require('customize-cra');
const { addReactRefresh } = require('customize-cra-react-refresh');

module.exports = override(
  addReactRefresh({ disableRefreshCheck: true }),
  ...addBabelPlugins('@babel/plugin-proposal-class-properties'),
  babelInclude([
    path.resolve(__dirname, 'node_modules/react-native-elements'),
    path.resolve(__dirname, 'node_modules/react-native-vector-icons'),
    path.resolve(__dirname, 'node_modules/react-native-ratings'),
    path.resolve(__dirname, 'node_modules/react-native-view-more-text'),
    path.resolve(__dirname, 'node_modules/react-native-popup-menu'),
    path.resolve(__dirname, 'src'),
  ])
);