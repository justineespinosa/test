import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createStateSyncMiddleware, initStateWithPrevTab, withReduxStateSync  } from "redux-state-sync";
import thunkMiddleware from 'redux-thunk';
import { logger } from 'redux-logger';
import throttle from 'lodash/throttle';
import { PERSISTED_STATE_KEY } from './config/settings';

//Modules imports
import Authentication from './views/containers/Authentication/state';
import User404 from './views/containers/Errors/User404';
import Admin404 from './views/containers/Errors/Admin404';

//Users
import UserMain from './views/containers/UserMain/state';
import Top from './views/containers/Top/state';
import MyPage from './views/containers/MyPage/state';
import Chat from './views/containers/Chat/state';
import Search from './views/containers/Search/state';
import Notifications from './views/containers/Notifications/state';
import Post from './views/containers/Post/state';

//Admin
import AdminMain from './views/containers/AdminMain/state';
import UserManagement from './views/containers/UserManagement/state';
import NotificationManagement from './views/containers/NotificationManagement/state';
import RandomMessage from './views/containers/RandomMessage/state';
import Maintenance from './views/containers/Maintenance/state';

//Store methods
const loadState = () => {
    try {
        const serializedState = localStorage.getItem(PERSISTED_STATE_KEY);
        return null === serializedState ? undefined : JSON.parse(serializedState);
    } catch (e) { return undefined; }
};

const saveState = (state) => {
    try {
        localStorage.setItem(PERSISTED_STATE_KEY, JSON.stringify(state));
    } catch (e) { }
};

//Module Reducers
const rootReducer = combineReducers({
    usermain: UserMain.reducer,
    auth: Authentication.reducer,
    top: Top.reducer,
    mypage: MyPage.reducer,
    chat: Chat.reducer,
    search: Search.reducer,
    notifications: Notifications.reducer,
    randommsg: RandomMessage.reducer,
    usermanagement: UserManagement.reducer,
    notifmanagement: NotificationManagement.reducer,
    maintenance: Maintenance.reducer,
    post: Post.reducer
});

//Root Routes
export const rootRoutes = [
    ...UserMain.routes,
    ...Authentication.routes,
    ...AdminMain.routes,
    { component: User404 }
]

//User Module Routes
export const userRoutes = [
    //Insert user module routes here
    ...Top.routes,
    ...MyPage.routes,
    ...Chat.routes,
    ...MyPage.routes,
    ...Search.routes,
    ...Notifications.routes,
    ...Post.routes,
    { component: User404 }
]

//Admin Module Routes
export const adminRoutes = [
    //Insert admin module routes here
    ...UserManagement.routes,
    ...NotificationManagement.routes,
    ...RandomMessage.routes,
    ...Maintenance.routes,
    { component: Admin404 }
]

const persistedState = loadState();

const middleware = applyMiddleware(
    createStateSyncMiddleware({
        broadcastChannelOption: { type: 'localstorage' },
    }),
    thunkMiddleware,
    logger
)

const store = createStore(
    withReduxStateSync(rootReducer),
    persistedState,
    middleware,
);

initStateWithPrevTab(store);

export default function configureStore() {
    store.subscribe(throttle(() => {
        saveState(store.getState());
    }), 1000);

    return store;
};
