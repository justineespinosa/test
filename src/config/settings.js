//Dummy json data
export const REST_SERVICE_URL_ROOT = process.env.REACT_APP_API_URL;
//Persisted key 
export const PERSISTED_STATE_KEY = process.env.REACT_APP_PERSISTED_KEY;
//Client Id
export const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
//Client Secret
export const CLIENT_SECRET = process.env.REACT_APP_CLIENT_SECRET;
//Root
export const ROOT = '/';

//VIMEO URL
export const VIMEO_PLAYER_URL = "https://player.vimeo.com";

//Fumiya ID
export const FUMIYA_USER_ID = 2;