//Add Support for IE Versions
import 'react-app-polyfill/ie11';
import 'react-app-polyfill/stable';

import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import configureStore, { rootRoutes } from './app.modules.js';
import {
    BrowserRouter,
    Switch
} from 'react-router-dom';
import PublicRoute from "./views/components/PublicRoute";
import ReactBreakpoints from 'react-breakpoints';
import './includes/bootstrap/css/bootstrap.min.css';
import './styles/css/index.css';
import './styles/css/feed.css';
import './styles/css/video.css';
import './styles/css/custom-bootstrap.css';

const breakpoints = {
    mobile: 320,
    mobileLandscape: 638,
    tablet: 768,
    tabletLandscape: 1024,
    desktop: 1200,
    desktopLarge: 1500,
    desktopWide: 1920,
}

const Root = (props) => {
    return (
        <ReactBreakpoints breakpoints={breakpoints}>
            <style type="text/css">{`
                @font-face {
                font-family: 'MaterialIcons';
                src: url(${require('react-native-vector-icons/Fonts/MaterialIcons.ttf')}) format('truetype');
                }

                @font-face {
                font-family: 'FontAwesome';
                src: url(${require('react-native-vector-icons/Fonts/FontAwesome.ttf')}) format('truetype');
                }
                
                @font-face {
                font-family: 'Ionicons';
                src: url(${require('react-native-vector-icons/Fonts/Ionicons.ttf')}) format('truetype');
                }
            `}
            </style>
            <Provider store={configureStore()}>
                <BrowserRouter initialEntries={[props.currentPath]}>
                    <Switch>
                        {rootRoutes.map((route, i) => <PublicRoute key={i} {...route} />)}
                    </Switch>
                </BrowserRouter>
            </Provider>
        </ReactBreakpoints>
    )
}

ReactDOM.render(
    <Root />,
    document.getElementById('root')
);