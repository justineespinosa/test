import Maintenance from "./Maintenance";

export const routes = [
    {
        path: "/maintenance",
        component: Maintenance,
        exact: true,
        showNav: true
    }
]