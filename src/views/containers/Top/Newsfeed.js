import React, { Component } from 'react';
import { Card, Image } from 'react-bootstrap';
import { appendUserRoute } from '../../../utils/common';
import { Link } from 'react-router-dom';
import ProfileIcon from '../../../includes/img/placeholders/user.svg';
import CameraIcon from '../../../includes/img/icons/camera.svg';
import PCOnlyView from '../../components/PCOnlyView';
import PostItemTemplate from '../../components/PostItemTemplate';
import './styles/create.scss';
import { NewsfeedList } from './dummy';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";

const CreatePostArea = () => {
    let appendedRoute = appendUserRoute("/mypage");
    let createPostRoute = appendUserRoute("/post/create");

    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                return (
                    <Card className="feed-container-item" style={mobileView ? { border: 'none' } : {}}>
                        <PCOnlyView>
                            <span className="feed-card-header">Create Post</span>
                        </PCOnlyView>

                        <Card.Body className="feed-card-container">
                            <Link key={appendedRoute + "main"} href={appendedRoute} to={appendedRoute} className="user-main-profile mr-3">
                                <Image src={ProfileIcon} style={{ width: 36, height: 36 }} roundedCircle />
                            </Link>

                            <Link key={createPostRoute + "input"} href={createPostRoute} to={createPostRoute} className="feed-card-create">
                                <input
                                    className="input-radius"
                                    style={{ width: '100%', height: 32, fontSize: 14 }}
                                    type="search"
                                    placeholder={"検索する"}
                                    contentEditable={false}
                                />
                            </Link>

                            <Link key={createPostRoute + "camera"} href={createPostRoute} to={createPostRoute} className="ml-3">
                                <Image src={CameraIcon} style={{ width: 36, height: 36, marginRight: 10 }} />
                            </Link>


                        </Card.Body>
                    </Card>
                )
            }}
        </Media>
    )

}

class Newsfeed extends Component {
    render() {

        return (
            <React.Fragment>
                <CreatePostArea />
                {NewsfeedList.map((details,i) => <PostItemTemplate key={"post"+i} {...details} />)}

            </React.Fragment>
        );
    }
}

export default Newsfeed;