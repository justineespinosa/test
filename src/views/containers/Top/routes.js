import Newsfeed from "./Newsfeed";
import HamburgerMenu from "../../components/HamburgerMenu";

export const routes = [
    {
        path: "/newsfeed",
        component: Newsfeed,
        exact: true,
        showNav: true
    },
    {
        path: "/menu",
        component: HamburgerMenu,
        exact: true,
        showNav: true
    }
]