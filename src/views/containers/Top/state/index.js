import reducer from "./reducers";
import { routes } from "../routes";

export default {
    reducer,
    routes
};
