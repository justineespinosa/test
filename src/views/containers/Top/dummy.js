let dummyMessage = `テキストテキストテキストテキストテキストテキストテキストテキストテキ
        ストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス
        トテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
        テキストテキストテキストテキストトテキストテキストテキストテキストテキストトテキストテキス
        トテキストテキストテキストトテキストテキストテキストトテキストテキストテキストトテキストテ
        テキストテキストテキストテキストテキストテキストテキストテキストテキトテキストテキトテキス
        ストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキス
        トテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト
        テキストテキストテキストテキストトテキストテキストテキストテキストテキストトテキストテキス
        トテキストテキストテキストトテキストテキストテキストトテキストテキストテキストトテキストテ`;
let dummyComment = [
    {
        _id: "c1001",
        commentBody: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ",
        author: {
            userId: 1,
            handleName: "Dummy Account 1",
            profileImageUrl: "https://cdn.dribbble.com/users/29574/screenshots/4877879/avatar_-_asia_18_-_dribbble.png",
        },
        createDt: "2020/04/19 1:30",
        updateDt: "2020/04/19 1:40",
    },
    {
        _id: "c1001",
        commentBody: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ",
        author: {
            userId: 2,
            handleName: "Dummy Account 2",
            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
        },
        createDt: "2020/04/25 1:30"
    }
]

export const NewsfeedList = [
    {
        postId: "1",
        type: 1,
        commentCount: 5,
        createDt: "2020/04/19 1:30",
        author: {
            userId: 2,
            handleName: "Dummy Account",
            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
            isUserDeleted: false,
            isEdited: true
        },
        postBody: dummyMessage,
        vimeoLivestreamId: "409639327",
        startLivestreamDt: "2020-04-20",
        endLivestreamDt: "2020-04-20",
    },
    {
        postId: "12",
        type: 0,
        attachmentType: 2,
        commentCount: 10,
        source: 0,
        createDt: "2020/04/16 11:30",
        author: {
            userId: 2,
            handleName: "Dummy Account",
            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
            isUserDeleted: false,
            isEdited: true
        },
        postBody: dummyMessage,
        videoUrl: "/video/409647091",
        videoStatus: 1,
        comments: [
            {
                _id: "c1001",
                commentBody: dummyMessage,
                author: {
                    userId: 2,
                    handleName: "Dummy Account 1",
                    profileImageUrl: "https://cdn.dribbble.com/users/29574/screenshots/4877879/avatar_-_asia_18_-_dribbble.png",
                },
                createDt: "2020/04/19 1:30",
                updateDt: "2020/04/19 1:40",
                replies: [
                    {
                        _id: "r1001",
                        replyBody: "テキストテキストテキストテキストテキストテキ",
                        author: {
                            userId: 2,
                            handleName: "Dummy Account",
                            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
                            isUserDeleted: false,
                            isEdited: true
                        },
                        createDt: "2020/04/19 1:40",
                        updateDt: "2020/04/19 1:45"
                    },
                    {
                        _id: "r1001",
                        replyBody: "テキストテ",
                        author: {
                            userId: 2,
                            handleName: "Dummy Account",
                            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
                            isUserDeleted: false,
                            isEdited: true
                        },
                        createDt: "2020/04/19 1:40"
                    }
                ]
            },
            {
                _id: "c1002",
                commentBody: "テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキ",
                author: {
                    userId: 1,
                    handleName: "Dummy Account 2",
                    profileImageUrl: "https://cdn.dribbble.com/users/29574/screenshots/4877879/avatar_-_asia_18_-_dribbble.png",
                },
                createDt: "2020/04/21 10:15",
                // updateDt: "2020/04/21 11:36",
                replies: [
                    {
                        _id: "r1001",
                        replyBody: "テキストテキストテキスキストテキ",
                        author: {
                            userId: 2,
                            handleName: "Dummy Account",
                            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
                            isUserDeleted: false,
                            isEdited: true
                        },
                        createDt: "2020/04/19 1:40",
                        updateDt: "2020/04/19 1:45"
                    },
                    {
                        _id: "r1001",
                        replyBody: "テストテキ",
                        author: {
                            userId: 2,
                            handleName: "Dummy Account",
                            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
                            isUserDeleted: false,
                            isEdited: true
                        },
                        createDt: "2020/04/19 1:40",
                        updateDt: "2020/04/19 1:45"
                    }
                ]
            }
        ]
    },
    {
        postId: "2",
        type: 2,
        attachmentType: 1,
        // isCommunity: true,
        // communityId: "2",
        createDt: "2020/04/15 8:22",
        updateDt: "2020/04/15 8:45",
        commentCount: 1,
        title: "日記タイトルテキスト",
        source: 1,
        author: {
            userId: 1,
            handleName: "Dummy Account",
            profileImageUrl: "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTlwVcBdsBhEblPanPg4OpBsoUxloEeAnUOoa8W56thKVpceMrg&usqp=CAU",
            isUserDeleted: false,
            isEdited: true
        },
        postBody: dummyMessage,
        comments: [...dummyComment, ...dummyComment],
        photos: [
            { title: "Title #1", body: dummyMessage, comments: dummyComment, imageUrl: "https://images-na.ssl-images-amazon.com/images/I/C1gxpfyEnnS._SL1000_.png", imageStatus: 1 },
            { title: "Title #2", body: dummyMessage, imageUrl: "https://images-na.ssl-images-amazon.com/images/I/C1gxpfyEnnS._SL1000_.png", imageStatus: 1 },
            { title: "Title #3", body: dummyMessage, imageUrl: "https://images-na.ssl-images-amazon.com/images/I/C1gxpfyEnnS._SL1000_.png", imageStatus: 1 },
            { title: "Title #4", body: dummyMessage, comments: dummyComment, imageUrl: "https://images-na.ssl-images-amazon.com/images/I/C1gxpfyEnnS._SL1000_.png", imageStatus: 1 },
            { title: "Title #4", body: dummyMessage, comments: dummyComment, imageUrl: "https://images-na.ssl-images-amazon.com/images/I/C1gxpfyEnnS._SL1000_.png", imageStatus: 0 },
            // { imageUrl: "https://up.gc-img.net/post_img_web/2013/07/WXpcR3qOZbgGjMX_5512.jpeg", imageStatus: 1 }, 
        ]
    },
    {
        postId: "3",
        createDt: "2020/04/15 8:05",
        author: {
            userId: 1,
            handleName: "Dummy Account",
            profileImageUrl: "https://cdn.dribbble.com/users/29574/screenshots/4877879/avatar_-_asia_18_-_dribbble.png"
        },
        postBody: dummyMessage,
        attachmentType: 2,
        videoUrl: "video/391162938",
        videoStatus: 2,
    }
]