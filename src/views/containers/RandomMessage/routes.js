import RandomMessageList from "./RandomMessageList";

export const routes = [
    {
        path: "/random-message",
        component: RandomMessageList,
        exact: true,
        showNav: true
    }
]