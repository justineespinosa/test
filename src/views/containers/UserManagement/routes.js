import UserSearch from "./UserSearch";

export const routes = [
    {
        path: "/user-search",
        component: UserSearch,
        exact: true,
        showNav: true
    }
]