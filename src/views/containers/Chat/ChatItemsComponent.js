import React from "react";
import { View, Text } from "react-native";
import styles from "./styles/chatconversation.styles.native";
import "./styles/chat.scss";
import moment from "moment";
import RemoveModalWindow from "./RemoveModalWindow"
import PCOnlyView from "../../components/PCOnlyView"
import MobileOnlyView  from "../../components/MobileOnlyView"
import { CHAT_TYPE } from "../../../utils/constants"

const ChatItemsComponent = props => {
  return (
    <View>
      <Text style={styles.textCenter}>
        {moment(props.date).format("YYYY/MM/DD")}
      </Text>
      {props.item.map(item => {
        if (props.ownerId === item.senderId) {
          return (
            <UserItemContainer key={item.chatId.$oid} {...props} item={item} />
          );
        } else {
          return (
            <FriendItemContainer key={item.chatId.$oid} {...props} item={item} />
          );
        }
      })}
    </View>
  );
};

const Timestamp = props => {
  return (
    moment(props.item.createDt.$date).get("hours") +
    ":" + moment(props.item.createDt.$date).get("minutes")
  );
};

const UserItemContainer = props => {
  return (
    <View
      style={
        props.item.chatId !== props.selectedMessage
          ? styles.belowContainer
          : null
      }
    >
      {CHAT_TYPE.text === props.item.type ? (
        <View>
          {!props.item.isDeleted ? (
              <View style={styles.userItemContainer}>
                <MobileOnlyView>
                      <Text style={styles.userTimeStamp}>
                        {props.item.isDeleted ? null : <Timestamp {...props} />}
                      </Text>
                      <Text
                        style={styles.userChatItem}
                        onPress={() => props.handleToggleDelete(props.item.chatId)}
                      >
                        {props.item.chatBody}
                      </Text>
                </MobileOnlyView>
                <PCOnlyView>
                     <Text style={styles.userTimeStampPC}>
                        {props.item.isDeleted ? null : <Timestamp {...props} />}
                      </Text> 
                      <Text
                        style={styles.userChatItemPC}
                        onPress={() => props.handleToggleDelete(props.item.chatId)}
                      >
                        {props.item.chatBody}
                      </Text>
                </PCOnlyView>
            </View>
    
          ) : (
            <Text style={styles.deletedMessage}>メッセージを削除しました。</Text>
          )}
          {props.isMessageDelete &&  props.item.chatId === props.selectedMessage ? (
            <View style={styles.chatDeleteContainer}>
               <RemoveModalWindow {...props}/>
            </View>
          ) : null}
        </View>
      ) : (
        <View style={styles.userItemContainer}>
          <Text style={styles.userTimeStamp}>
            {props.item.isDeleted ? null : <Timestamp {...props} />}
          </Text>

          {!props.item.isDeleted ? (
             <View>
             <MobileOnlyView>
                <div className="chat-image-container">
                  <img
                    alt="friend-item"
                    src={"https://www.fillmurray.com/100/200"}
                  />
                </div>
             </MobileOnlyView>
             <PCOnlyView>
                <div className="chat-image-pc-container">
                  <img
                    alt="friend-item"
                    src={"https://www.fillmurray.com/100/200"}
                    onClick={() => props.handleToggleDelete(props.item.chatId)}
                  />
                </div>
             </PCOnlyView>
           </View>
          ) : (
            <Text style={styles.deletedMessage}>メッセージを削除しました。</Text>
          )}
          {props.isMessageDelete &&
          props.item.chatId === props.selectedMessage ? (
            <View style={styles.chatDeleteContainer}>
              <RemoveModalWindow {...props}/>
            </View>
          ) : null}
        </View>
      )}
    </View>
  );
};

const FriendItemContainer = props => {
  return (
    <View style={styles.belowContainer}>
      {0 === props.item.type ? (
        <View>
          {!props.item.isDeleted ? (
            <View style={styles.friendItemContainer}>
               <MobileOnlyView>
                      <Text style={styles.friendChatItem}>
                        {props.item.chatBody}
                      </Text>
                </MobileOnlyView>
                <PCOnlyView>
                      <Text style={styles.friendChatItemPC}>
                        {props.item.chatBody}
                      </Text>
                </PCOnlyView>
              {props.item.isDeleted ? null : (
                <View style={styles.friendTimeStampContainer}>
                  <MobileOnlyView>
                        <Text style={styles.friendTimeStamp}>
                          <Timestamp {...props} />
                        </Text>
                  </MobileOnlyView>
                  <PCOnlyView>
                    <Text style={styles.friendTimeStampPC}>
                      <Timestamp {...props} />
                    </Text>
                  </PCOnlyView>
                </View>
              )}
            </View>
          ) : (
            <Text style={styles.deletedMessage}>
              {props.recipientHandleName}さんがメッセージを削除しました。
            </Text>
          )}
        </View>
      ) : (
        <View style={styles.friendItemContainer}>
          {!props.item.isDeleted ? (
            <View>
             <MobileOnlyView>
                <div className="chat-image-container">
                  <img
                    alt="friend-item"
                    src={"https://www.fillmurray.com/100/200"}
                  />
                </div>
             </MobileOnlyView>
             <PCOnlyView>
                <div className="chat-image-pc-container">
                  <img
                    alt="friend-item"
                    src={"https://www.fillmurray.com/100/200"}
                  />
                </div>
             </PCOnlyView>
           </View>
          ) : (
            <Text style={styles.deletedMessage}>
              Message deleted by {props.item.senderId}
            </Text>
          )}
          {props.item.isDeleted ? null : (
            <Text style={styles.friendTimeStamp}>
              <Timestamp {...props} />
            </Text>
          )}
        </View>
      )}
    </View>
  );
};

export default ChatItemsComponent;
