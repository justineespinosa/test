import React from "react";
import "./styles/radio.scss";

const RadioButton = props => {
  if (props.isThreadDelete) {
    return (
      <label className="label-container">
        <input
          className="radio-btn"
          id={props.id}
          type="radio"
          onChange={() => props.handleSelectThread(props.id)}
          checked={
            props.isThreadDelete && props.id === props.selectedThread
              ? true
              : false
          }
        />
        <span className="checkmark"></span>
      </label>
    );
  } else {
    return null;
  }
};

export default RadioButton;
