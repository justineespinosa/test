import React from "react";
import { KeyboardAvoidingView, TextInput, Text, TouchableOpacity, Image } from "react-native";
import SendIcon from "../../../includes/img/icons/send.svg";
import GalleryIcon from "../../../includes/img/icons/image.svg";
import styles from "./styles/chatconversation.styles.native";
import { isNullOrUndefined } from "util";
import { EMPTY_STRING, CHAT_MAX_CHAR } from "../../../utils/constants";
import './styles/chat.scss'
import {isFumiyaUserCheck} from "../../../utils/common"

const SendChatComponent = (props) => {
  
  function CustomError(column) {
    return !isNullOrUndefined(props.errors[column]) ? <span className="form-error">{props.errors[column]}</span> : "";
  }
  
  return (
    <KeyboardAvoidingView style={styles.inputContainer}>
      <TouchableOpacity onPress={() => props.handleUpdateAddImage()}>
        <Image source={GalleryIcon} style={styles.icon} />
      </TouchableOpacity>
        <TextInput 
          maxLength={CHAT_MAX_CHAR} 
          multiline style={styles.input} 
          onChangeText={(e)=>props.handleInputChange(e)} 
          placeholder="メッセージ" 
        />
      <Text>{CustomError("image") || CustomError("message")}</Text>
      <TouchableOpacity 
        onPress={() => props.handleSendMessage()}
        disabled={(isNullOrUndefined(props.message) || EMPTY_STRING === props.message)
        && props.imageFiles.length === 0 ? true : false && !isFumiyaUserCheck(props.recipientId) }>
        <Image source={SendIcon} style={styles.icon} />
      </TouchableOpacity>
    </KeyboardAvoidingView>
  );
};

export default SendChatComponent;
