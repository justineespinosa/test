import React from "react";
import { View, TextInput, TouchableOpacity, Image } from "react-native";
import SendIcon from "../../../includes/img/icons/send.svg";
import GalleryIcon from "../../../includes/img/icons/image.svg";
import styles from "./styles/chatconversation.styles.native";

const SendMessageComponent = () => {
  return (
    <View style={styles.inputContainer}>
      <TouchableOpacity>
        <Image source={GalleryIcon} style={styles.icon} />
      </TouchableOpacity>
      <TextInput style={styles.input} placeholder="メッセージ" />
      <TouchableOpacity>
        <Image source={SendIcon} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};

export default SendMessageComponent;
