export const SET_THREAD_LIST = "chat/SET_THREAD_LIST";
export const SET_FRIENDS_LIST = "chat/SET_FRIENDS_LIST";
export const SET_CHAT_THREAD = "chat/SET_CHAT_THREAD";
