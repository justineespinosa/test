import * as types from "./types";

export const setThreadList = data => ({
  type: types.SET_THREAD_LIST,
  payload: data
});

export const setFriendsList = data => ({
  type: types.SET_FRIENDS_LIST,
  payload: data
});

export const setChatThread = data => ({
  type: types.SET_CHAT_THREAD,
  payload: data
});
