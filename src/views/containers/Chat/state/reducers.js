import * as types from "./types";

const defaultState = {
  chatThreads: [],
  friendsList: [],
  threadMessages: []
};

export default function reducer(state = defaultState, action) {
  switch (action.type) {
    case types.SET_THREAD_LIST:
      return {
        ...state,
        chatThreads: action.payload
      };
    case types.SET_FRIENDS_LIST:
      return {
        ...state,
        friendsList: action.payload
      };
    case types.SET_CHAT_THREAD:
      return {
        ...state,
        threadMessages: action.payload
      };
    default:
      return state;
  }
}
