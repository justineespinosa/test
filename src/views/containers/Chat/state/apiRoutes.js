export const GET_ALL_FRIENDS = "https://m9ajmbnys7.execute-api.ap-northeast-1.amazonaws.com/api-chat/get_all_friends";
export const SEARCH_FRIEND = "https://m9ajmbnys7.execute-api.ap-northeast-1.amazonaws.com/api-chat/search_friend";
export const GET_ALL_CHAT_THREAD = "https://m9ajmbnys7.execute-api.ap-northeast-1.amazonaws.com/api-chat/get_all_chat_thread";
export const DELETE_CHAT_THREAD = "https://m9ajmbnys7.execute-api.ap-northeast-1.amazonaws.com/api-chat/delete_chat_thread";
export const GET_SPECIFIC_CHAT_THREAD = "https://m9ajmbnys7.execute-api.ap-northeast-1.amazonaws.com/api-chat/get_specific_chat_thread";
