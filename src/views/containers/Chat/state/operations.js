import * as Path from "./apiRoutes";
import ApiService from "../../../../utils/apiService";
import * as Actions from "./actions";
import { API_SUCCESS_STATUS, WEBSOCKET_ACTIONS } from "../../../../utils/constants";
import { isNullOrUndefined } from "util";

const getAllChatThread = (ownerId, currentPagination) => dispatch => {
  return ApiService.get( Path.GET_ALL_CHAT_THREAD + `/${ownerId}/${currentPagination}`)
    .then(response => {
      let data = JSON.parse(response.data.data);
      if (!isNullOrUndefined(response.data.status) && API_SUCCESS_STATUS === response.data.status) {
         dispatch(Actions.setThreadList(data));
      }
      return response.data;
    })
    .catch(error => {
      console.error(error);
    });
};

const deleteChatThread = threadId => dispatch => {
  //Actual API Call
  let params = { threadId: threadId.$oid };
  return ApiService.post(Path.DELETE_CHAT_THREAD, params)
    .then(response => {
      if (
        !isNullOrUndefined(response.data.status) &&
        API_SUCCESS_STATUS === response.data.status
      ) {
        return response.data;
      }
    })
    .catch(error => {
      console.error(error);
    });
};

const getAllFriends = (userId, currentPagination) => dispatch => {
    return ApiService.get(Path.GET_ALL_FRIENDS + `/${userId}/${currentPagination}`)
      .then(response => {
        let data = JSON.parse(response.data.data);
        if (!isNullOrUndefined(response.data.status) && API_SUCCESS_STATUS === response.data.status) {
            dispatch(Actions.setFriendsList(data));
        }
        return response.data;
      })
      .catch(error => {
        console.error(error);
      });
};

const searchFriend = searchModel => dispatch => {
  //Actual API Call
  let params = {
  "userId" : searchModel.userId,
  "handleName" : searchModel.handleName,
  "currentPagination" : searchModel.currentPagination
  }

  return ApiService.post(Path.SEARCH_FRIEND, params)
    .then(response => {
       let data = JSON.parse(response.data.data);
       if (!isNullOrUndefined(response.data.status) && API_SUCCESS_STATUS === response.data.status) {
          dispatch(Actions.setFriendsList(data));
        }
       return response.data;
    })
    .catch(error => {
      console.error(error);
    });
};

const getSpecificChatThread = (threadId, currentPagination, ownerId, recipientId) => dispatch => {
  let params = {
    ownerId: ownerId,
    recipientId: recipientId,
    currentPagination: currentPagination,
    threadId: isNullOrUndefined(threadId) ? null : threadId.$oid
  };

  return ApiService.post(Path.GET_SPECIFIC_CHAT_THREAD, params)
  .then(response => {
      let data = JSON.parse(response.data.data);
      if (!isNullOrUndefined(response.data.status) && API_SUCCESS_STATUS === response.data.status) {
        dispatch(Actions.setChatThread(data));
      }
      return response.data;
    })
    .catch(error => {
      console.error(error);
    });
};

const sendMessage = (messageModel, data, ws) => dispatch => {
  data.map((item, index) => {
    if (item instanceof File) {
      console.log("File", index);
    } else if(!isNullOrUndefined(item)) {
      ws.send(
        JSON.stringify({
          action: WEBSOCKET_ACTIONS.onMessage,
          type: WEBSOCKET_ACTIONS.onMessage,
          data: {
            chat: messageModel,
          },
        })
      );
    }
    return null;
  })
}

const deleteMessage = (messageModel, ws) => dispatch => {
  ws.send(
    JSON.stringify({
      action: WEBSOCKET_ACTIONS.onMessageDelete,
      type: WEBSOCKET_ACTIONS.onMessageDelete,
      data: {
        chatId : messageModel.chatId.$oid
      },
    })
  );
};

const updateChangeSeenStatus = (ownerId, recipientId, ws) => dispatch => {
  ws.send(
    JSON.stringify({
      action: WEBSOCKET_ACTIONS.updateSeenStatus,
      type: WEBSOCKET_ACTIONS.updateSeenStatus,
      data: {
        ownerId: ownerId,
        recipientId: recipientId
      },
    })
  );
};

export {
  getAllFriends,
  searchFriend,
  getAllChatThread,
  deleteChatThread,
  getSpecificChatThread,
  sendMessage,
  deleteMessage,
  updateChangeSeenStatus
};
