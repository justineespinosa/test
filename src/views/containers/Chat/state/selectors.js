import { CHAT_MAX_CHAR, EMPTY_STRING, REQUIRED_FIELD, INVALID_IMAGE, ACCEPTED_IMAGE_TYPES } from '../../../../utils/constants';

export const validateChatMessage = (message) =>{
    let errors = {};

    if(EMPTY_STRING === message){
        errors.message = REQUIRED_FIELD
    }
    if(CHAT_MAX_CHAR < message.length){
        errors.message = REQUIRED_FIELD
    }
    
    return errors;
}

export const validateChatImage = (file) =>{
    let errors = {};

    if(!file && "image" !== file['type'].split('/')[0]){
        errors.image = INVALID_IMAGE
    }

    if(!ACCEPTED_IMAGE_TYPES.includes(file['type'])){
        errors.image = INVALID_IMAGE
    }
    
    return errors;
}