import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  header: {
    backgroundColor: "#F5F5F5",
    borderBottomColor: "#F3f3f3",
    borderBottomWidth: 2
  },
  showSearchBtn: {
    width: "70%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderRadius: 20,
    borderColor: "#D6DBDF",
    borderWidth: 1
  },
  input: {
    padding: 8,
    width: "100%",
    borderRadius: 20
  },
  searchIcon: {
    height: 22,
    width: 22,
    marginRight: 15,
    marginLeft: 5
  },
  searchContainer: {
    flex: 1,
    alignItems: "center",
    padding: 10,
    paddingBottom: 0,
    backgroundColor: "#ffffff"
  },
  horizontalBorder: {
    borderBottomColor: "#EAEDED",
    borderBottomWidth: 1,
    marginTop: 10,
    width: "100%",
    alignSelf: "center"
  }
});

export default styles;
