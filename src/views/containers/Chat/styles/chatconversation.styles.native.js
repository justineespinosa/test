import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  chatListContainer: {
    backgroundColor: "#ffffff",
    zIndex: -9999,
    position: "relative"
  },
  listItemContainer: {
    padding: 10
  },
  listItem: {
    padding: 0,
    margin: 0
  },
  listContainer: {
    height: 600
  },
  title: {
    marginLeft: 20,
    color: "#0A182C"
  },
  horizontalBorder: {
    borderBottomColor: "#EAEDED",
    borderBottomWidth: 1,
    width: "100%",
    alignSelf: "center",
    padding: 5
  },
  inputContainer: {
    flex: 1,
    flexDirection: "row"
  },
  input: {
    width: "85%",
    height: 30,
    borderRadius: 20,
    borderColor: "gray",
    borderWidth: 1,
    backgroundColor: "white",
    marginTop: 10,
    marginBottom: 10,
    alignSelf: "center",
    paddingLeft: 12,
    paddingTop: 5
  },
  icon: {
    width: 25,
    height: 25,
    margin: 15
  },
  sendMessageContainer: {
    backgroundColor: "#F5F5F5"
  },
  userChatItemPC: {
    textAlign: "left",
    backgroundColor: "#F1F5FA",
    color: "black",
    borderRadius: 3,
    padding: 8,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    maxWidth: 300,
    fontSize: 14
  },
  userChatItem: {
    textAlign: "left",
    backgroundColor: "#F1F5FA",
    color: "black",
    borderRadius: 3,
    padding: 8,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    maxWidth: 200,
    fontSize: 14
  },
  userItemContainer: {
    flex: 1,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    flexDirection: "row"
  },
  friendChatItem: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#D7D8D9",
    borderRadius: 3,
    padding: 8,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    maxWidth: 200,
    fontSize: 14
  },
  friendChatItemPC: {
    backgroundColor: "white",
    borderWidth: 1,
    borderColor: "#D7D8D9",
    borderRadius: 3,
    padding: 8,
    marginRight: 5,
    marginLeft: 5,
    marginTop: 5,
    marginBottom: 5,
    maxWidth: 300,
    fontSize: 14
  },
  friendItemContainer: {
    flex: 1,
    maxWidth: 400,
    alignItems: "flex-start",
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  friendTimeStampContainer: {
    alignSelf: "flex-end"
  },
  friendTimeStamp: {
    textAlign: "right",
    color: "#AFAFB0",
    fontSize: 8,
    marginBottom: 5,
    textAlignVertical: "bottom"
  },
  friendTimeStampPC: {
    textAlign: "right",
    alignSelf: "flex-end",
    color: "#AFAFB0",
    fontSize: 10,
    marginBottom: 5,
    textAlignVertical: "bottom"
  },
  userTimeStamp: {
    textAlign: "right",
    fontSize: 8,
    marginBottom: 5,
    color: "#AFAFB0"
  },
  userTimeStampPC: {
    textAlign: "right",
    fontSize: 10,
    marginBottom: 5,
    color: "#AFAFB0"
  },
  deletedMessage: {
    marginTop: 10,
    padding: 5,
    color: "gray",
    alignSelf: "center"
  },
  chatDeleteContainer: {
    position: "absolute",
    alignSelf: "flex-end",
    flex: 1,
    marginTop: 40,
    marginLeft: -10,
    backgroundColor: "#4A4A4D",
    borderRadius: 3,
    zIndex: 9999
  },
  touchableDelete: {
    flexDirection: "row"
  },
  textCenter: {
    color: "#98A6B5",
    fontSize: 12,
    alignSelf: "center",
    margin: 10,
    zIndex: -999,
    position: "relative"
  },
  belowContainer: {
    position: "relative",
    zIndex: -9999
  },
  deleteIcon: {
    width: 15,
    height: 18,
    marginTop: 5,
    marginLeft: 10
  },
  deleteText: {
    padding: 5,
    color: "white"
  },
  fileUploadContainer:{
    padding: 10,
    width: "100%",
    flex: 1,
    flexDirection: "column",
    height: 110
  },
  removeIcon: {
    height: 15, 
    width: 15, 
    margin: 0, 
    position: "absolute", 
    right: 0
  }
});

export default styles;
