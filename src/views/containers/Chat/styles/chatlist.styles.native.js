import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  listItemContainer: {
    paddingTop: 10,
    paddingBottom: 10,
    paddingRight: 12,
    paddingLeft: 12
  },
  listItemContainerPC: {
    paddingTop: 10,
    paddingBottom: 10
  },
  listItem: {
    padding: 0,
    paddingLeft: 2,
    margin: 0
  },
  listContainer: {
    height: 750
  },
  titlePC: {
    marginLeft: 20,
    color: "#0A182C"
  },
  title: {
    marginLeft: 20,
    color: "#0A182C",
    maxWidth: 300
  },
  actionContainer: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end"
  },
  newChatIcon: {
    height: 22,
    width: 22
  },
  menuIcon: {
    height: 7,
    width: 25,
    marginTop: 10,
    marginRight: 25,
    resizeMode: "stretch"
  },
  deleteIcon: {
    height: 23,
    width: 21,
    marginRight: 10
  },
  horizontalBorder: {
    borderBottomColor: "#EAEDED",
    borderBottomWidth: 1,
    width: "100%",
    alignSelf: "center"
  },
  avatar: {
    width: 70,
    height: 70
  },
  cancelBtn: {
    backgroundColor: "transparent",
    borderRadius: 10,
    width: 47,
    height: 25,
    borderColor: "#707070",
    padding: 5
  },
  cancelBtnPC: {
    backgroundColor: "transparent",
    borderRadius: 5,
    width: 47,
    height: 25,
    borderColor: "#707070",
    padding: 5
  },
  cancelBtnTitle: {
    fontSize: 12,
    color: "#606770"
  },
  emptyList: {
    textAlign: "center",
    marginLeft: 20
  },
  seenChat: {
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft: 10,
  },
  unSeenChat: {
    backgroundColor: "#F4F4F4",
    paddingTop: 10,
    paddingBottom : 10,
    paddingLeft: 10
  },
  seenChatPC: {
    backgroundColor: "#FFFFFF",
    paddingLeft: 0,
    paddingTop: 5,
    paddingBottom: 5
  },
  unSeenChatPC: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 0,
    paddingTop: 10,
    paddingBottom: 10
  },
  seenChatItem:{
      backgroundColor: "#FFFFFF",
      padding: 0,
  },
  unseenChatItem: {
    backgroundColor: "#F4F4F4",
    padding: 0,
  }
});

export default styles;
