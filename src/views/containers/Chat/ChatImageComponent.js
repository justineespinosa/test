import React from "react"
import {TouchableOpacity, Image, FlatList} from "react-native"
import "./styles/chat.scss"
import {IMAGE_MAX_UPLOAD} from "../../../utils/constants"
import styles from "./styles/chatconversation.styles.native"
import AddImageIcon from "../../../includes/img/buttons/add-image.svg"
import CloseIcon from "../../../includes/img/icons/close.svg";

const ChatImageComponent = (props) => {
    return(
        <FlatList
            style={styles.fileUploadContainer}
            horizontal={true}
            data={[...props.imageFiles, {addImage: true}]}
            renderItem={({ item, index }) => {
                if (item.addImage && IMAGE_MAX_UPLOAD !== index) {
                  return (
                    <div className="file-upload-container">
                        <label className="custom-file-upload">
                            <img src={AddImageIcon} alt="icon-upload"/>
                            <input type="file" accept="image/x-png,image/gif,image/jpeg" capture="camera" onChange={(e) => props.handleAddImage(e)}/>
                        </label>               
                    </div>
                  );
                }else if(item instanceof File && IMAGE_MAX_UPLOAD !== index){
                    return (
                        <div className="image-preview-container">
                            <TouchableOpacity onPress={() => props.handleRemoveImage(index)}  >
                                <Image source={CloseIcon} alt="close-icon" style={styles.removeIcon} />
                            </TouchableOpacity>
                            <img className="image-preview" src={URL.createObjectURL(item)} alt={item.name}/>
                       </div>
                        );
                }else{
                    return null
                }
              }}
              keyExtractor={(item, index) => index.toString()}
              showsHorizontalScrollIndicator={false}
              extraData={props}
        />
    )
}

export default ChatImageComponent;