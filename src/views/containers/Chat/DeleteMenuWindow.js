import React from "react";
import { View, TouchableOpacity, Image, Text, StyleSheet } from "react-native";
import MessageIcon from "../../../includes/img/icons/messages.svg";

const DeleteMenuWindow = props => {
  if (props.isDeleteMenuShow) {
    return (
      <View style={styles.menuContainer}>
        <View style={styles.menuActionContainer}>
          <TouchableOpacity
            style={styles.menuTouchable}
            onPress={() => props.handleShowDeleteThread()}
          >
            <Image
              alt="pen-icon"
              source={MessageIcon}
              style={styles.messagesIcon}
            />
            <Text style={styles.menuText}>メッセージを選択</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  } else {
    return null;
  }
};

const styles = StyleSheet.create({
  menuText: {
    color: "white",
    padding: 5,
    paddingLeft: 0,
    fontSize: 10
  },
  menuActionContainer: {
    padding: 7,
    marginRight: 5,
    backgroundColor: "#000000",
    borderRadius: 3,
    opacity: 0.7
  },
  menuContainer: {
    position: "absolute",
    alignSelf: "flex-end",
    marginTop: 55,
    zIndex: 2
  },
  menuTouchable: {
    flex: 1,
    flexDirection: "row"
  },
  messagesIcon: {
    height: 15,
    width: 15,
    margin: 5,
    resizeMode: "stretch"
  }
});

export default DeleteMenuWindow;
