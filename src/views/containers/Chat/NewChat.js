import React, { Component } from "react";
import { View, TouchableOpacity, TextInput, Image } from "react-native";
import img from "../../../includes/img/icons/search.svg";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { chatOperations } from "./state";
import { EMPTY_STRING, API_ERROR_STATUS, ENTER_KEY, SEARCH_KEY } from "../../../utils/constants";
import UserListItemComponent from "../../components/UserListItemComponent";
import styles from "./styles/newchat.styles.native.js";
import "./styles/chat.scss";
import { isNullOrUndefined } from "util";

class NewChat extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.userId,
      searchFieldInput: EMPTY_STRING,
      currentPagination: 1,
      scrollEnd: true,
      friendsList: []
    };
  }

  componentDidMount() {
    this.props
      .getAllFriends(this.state.userId, this.state.currentPagination)
      .then(data => {
        if (!isNullOrUndefined(data) && API_ERROR_STATUS === data.status) {
          console.log(data.status);
        }else{
          this.setState({
            friendsList : this.props.friendsList
          })
        }
      });
  }

  handleInputChange = event => {
    this.setState({
      searchFieldInput: event
    });
  };

  handleOnKeyPress = event => {
    if (ENTER_KEY === event.nativeEvent.key || SEARCH_KEY === event.nativeEvent.key) {
      this.handleSearchFriend();
    }
  };

  handleSearchFriend = () => {
    this.setState({
      currentPagination : 1
    })

    const pagination = this.state.currentPagination;

    const searchModel = {
      userId: this.state.userId,
      handleName: this.state.searchFieldInput,
      currentPagination: pagination
    };

    this.props.searchFriend(searchModel).then(data => {
      if (API_ERROR_STATUS === data.status) {
        console.log(data.status);
      }else{
        this.setState({
          friendsList : this.props.friendsList
        })
      }
    });
  };

  handlePagination = () => {
    const searchModel = {
      userId: this.state.userId,
      handleName: this.state.searchFieldInput,
      currentPagination: this.state.currentPagination + 1
    };

    if (EMPTY_STRING === this.state.searchFieldInput) {
      this.props.getAllFriends(this.state.userId, this.state.currentPagination + 1)
        .then(data => {
          if (API_ERROR_STATUS === data.status) {
            console.log(data.status);
          }else{
            this.setState({
              friendsList :  [...this.state.friendsList, ...this.props.friendsList]
            })
          }
        });
    } else {
      this.props.searchFriend(searchModel).then(data => {
        if (API_ERROR_STATUS === data.status) {
          console.log(data.status);
        }else{
          this.setState({
            friendsList :  [...this.state.friendsList, ...this.props.friendsList]
          })
        }
      });
    }
    this.setState({
      currentPagination: this.state.currentPagination + 1
    });
  };

  handleSelectFriend = item => {
    this.props.history.push({
      pathname: "/app/chatmessages",
      state: {
        friendId: item.userId,
        friendHandleName: item.handleName,
        threadId: null
      }
    });
  };

  isAtBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height;
  };

  onScrollEnd = () => {
    this.setState({ scrollEnd: false });
  };

  render() {
    return (
      <div className="list-container">
        <View style={styles.header}>
          <p className="header-text">新規メッセージ</p>
        </View>
        <View>
          <View style={styles.searchContainer}>
            <View style={styles.showSearchBtn}>
              <TextInput
                placeholder="宛先"
                value={this.state.searchFieldInput}
                onChangeText={this.handleInputChange}
                onKeyPress={this.handleOnKeyPress}
                style={styles.input}
                returnKeyType="search"
              />
              <TouchableOpacity onPress={() => this.handleSearchFriend()}>
                <Image
                  alt="search-icon"
                  source={img}
                  style={styles.searchIcon}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.horizontalBorder} />
          </View>
          <div className="flatlist-container">
            <UserListItemComponent
              {...this.state}
              list={this.state.friendsList}
              isCommunity={false}
              handlePagination={this.handlePagination}
              handleSelectItem={this.handleSelectFriend}
              isAtBottom={this.isAtBottom}
              onScrollEnd={this.onScrollEnd}
            />
          </div>
        </View>
      </div>
    );
  }
}

//Bind dispatch to action creators
const mapStateToProps = state => {
  return {
    friendsList: state.chat.friendsList,
    userId: state.auth.credentials.userId
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllFriends: chatOperations.getAllFriends,
      searchFriend: chatOperations.searchFriend
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(NewChat);
