import React, { Component } from "react";
import { View, FlatList } from "react-native";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import SendChatComponent from "./SendChatComponent";
import ChatHeaderComponent from "./ChatHeaderComponent";
import ChatItemsComponent from "./ChatItemsComponent";
import "./styles/chat.scss";
import { chatOperations } from "./state";
import { API_ERROR_STATUS, EMPTY_STRING, WEBSOCKET_MESSAGES } from "../../../utils/constants";
import styles from "./styles/chatconversation.styles.native";
import { isNullOrUndefined } from "util";
import { selectors } from './state';
import { authOperations } from "../Authentication/state";
import ChatImageComponent from "./ChatImageComponent";
import moment from "moment"

class ChatConversation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ownerId: this.props.userId,
      ownerHandleName: this.props.handleName,
      recipientId: this.props.location.state.friendId,
      recipientHandleName: this.props.location.state.friendHandleName,
      threadId: this.props.location.state.threadId,
      isMessageDelete: false,
      selectedMessage: null,
      message: null,
      imageFiles: [],
      isImageUpload: false,
      currentPagination: 1,
      threadMessages: [],
      groupedMessages:[],
      errors: {},
      initialLoad: true,
    };
    this.ws = this.props.websocket
  }


  componentDidMount() {
    if(!isNullOrUndefined(this.ws) &&  this.ws.readyState === WebSocket.OPEN){
      this.ws.onopen = () =>{
        console.log("Connected");
      }

      this.ws.onmessage = (data) =>{
        let serverData = JSON.parse(data.data);
        if (WEBSOCKET_MESSAGES.onMessage === serverData.action) {
        } else if (WEBSOCKET_MESSAGES.onImageUpload === serverData.action) {
        } else if (WEBSOCKET_MESSAGES.onImageRender === serverData.action) {
        } else if (WEBSOCKET_MESSAGES.onMessageDelete === serverData.action) {
            //Transfer to websocket if sending is fixed
            this.setState(prevState => {
              const threadMessages = prevState.groupedMessages.map(item => {
                const messages = item.messages.map(item => {
                  if (item.chatId === this.state.selectedMessage) {
                    return {
                      ...item,
                      isDeleted: true
                    };
                  }
                  return item;
                });
                return { ...item.messages, messages };
              });
              return {
                groupedMessages: threadMessages
              };
            });

            this.setState({
              selectedMessage: null,
              isMessageDelete: false
            });
        }
      }
  
      this.ws.onerror = () =>{
        console.log("Error on Websocket API");
      }
  
      this.ws.onclose = () =>{
        console.log("Error on Websocket API");
      }
       //Update seen status using this.props.updateSeenStatus()
       this.props.updateChangeSeenStatus(this.state.ownerId, this.state.recipientId, this.ws)
    }

    this.props.getSpecificChatThread(
        this.state.threadId,
        this.state.currentPagination,
        this.state.ownerId,
        this.state.recipientId
      )
      .then(response => {
        if (!isNullOrUndefined(response) && API_ERROR_STATUS === response.status) {
          console.log(response.status);
        } else {
          // this gives an object with dates as keys
          this.setState({
            threadMessages: this.props.threadMessages,
          })
          this.handleTransformData();
        }
      });
  }

  handleTransformData = () => {
    this.state.threadMessages.map(item => {
      let dates = item.chat.reduce((day, item) => {
        const createdDt = JSON.stringify(moment(item.createDt.$date).format('L'));
        const convertedDt = createdDt.slice(1, 20);
        const actualDay = convertedDt;
        if (!day[actualDay]) {
          day[actualDay] = [];
        }
        day[actualDay].push(item);
        return day;
      }, {});

      // Edit: to add it in the array format instead
      let groupArrays = Object.keys(dates).map(date => {
        return {
          date,
          messages: dates[date]
        };
      });
      this.setState({
        groupedMessages: groupArrays
      });
    });
  };

  handleToggleDelete = id => {
    this.setState({
      selectedMessage: id,
      isMessageDelete: !this.state.isMessageDelete
    });
  };

  handleDeleteMessage = id => {
    //Call API if success proceed
    if(!isNullOrUndefined(this.ws) && this.ws.readyState === WebSocket.OPEN){
      const messageModel = {
        chatId : id
      }
      this.props.deleteMessage(messageModel, this.ws)
    }
    //Remove after socket sending is fixed
    this.setState(prevState => {
      const threadMessages = prevState.groupedMessages.map(item => {
        const messages = item.messages.map(item => {
          if (item.chatId === this.state.selectedMessage) {
            return {
              ...item,
              isDeleted: true
            };
          }
          return item;
        });
        return { ...item.messages, messages };
      });
      return {
        groupedMessages: threadMessages
      };
    });

    this.setState({
      selectedMessage: null,
      isMessageDelete: false
    });
  };

  handleInputChange = (e) => {
    this.setState({
      message: e
    })
  }

  handleSendMessage = (e) => {
    let data = [this.state.message, ...this.state.imageFiles]
    data.map((item) => {
      if(item instanceof File){
        let errors = selectors.validateChatImage(item)
        this.setState({
          errors
        })
      }else if(EMPTY_STRING !== this.state.message && !isNullOrUndefined(this.state.message)){
        let errors = selectors.validateChatMessage(item)
        this.setState({
          errors
        })
      }
      return null;
    })

    if(0 >= Object.keys(this.state.errors).length && !isNullOrUndefined(this.ws) && this.ws.readyState === WebSocket.OPEN){
        //model for chatbody and object for message and image files
        const messageModel = {
          ownerId: this.state.ownerId,
          recipientId: this.state.recipientId,
          recipientHandleName: this.state.recipientHandleName,
          ownerHandleName: this.state.ownerHandleName,
          type: 0,
          chatBody: this.state.message,
          senderId: this.state.ownerId,
        };
        this.props.sendMessage(messageModel, data, this.ws);
    }
  }

  handleUpdateAddImage = () => {
    this.setState({
      isImageUpload: !this.state.isImageUpload
    })
  }

  handleAddImage = (e) => {
    let imageFiles = e.target.files[0]
    this.setState({
      imageFiles: [...this.state.imageFiles, imageFiles]
    })
  }

  handleRemoveImage = (num) => {
    this.state.imageFiles.map((item, index) =>{
      if(index === num){
     
        let arr = this.state.imageFiles.filter(function(item, index) {
           return index !== num
      })
        this.setState({
          imageFiles : arr
        })
      }
      return null
    })
  }

  handlePagination = () =>{
    this.setState(prevState =>{
      return{
           ...prevState,
           currentPagination : prevState.currentPagination + 1,
           initialLoad : false
      }
   })

   this.props.getSpecificChatThread(this.state.threadId, this.state.currentPagination,this.state.ownerId
    ,this.state.recipientId).then((response) => {
      if (!isNullOrUndefined(response) && API_ERROR_STATUS === response.status) {
        console.log(response.status);
      } else {
        // this gives an object with dates as keys
        this.handleTransformData();
      }
    })
  }

  isAtTop = ({contentOffset}) =>{
    return contentOffset.y === 0
  }

  render() {
    return (
      <div className="list-container">
        <ChatHeaderComponent {...this.state} />
        <div className="message-container">
          <FlatList
            ref={(ref) => (this.flatlist = ref)}
            style={{ height: "100%" }}
            onScroll={({nativeEvent}) =>{
              if(this.isAtTop(nativeEvent)){
                this.handlePagination()
              }
            }}
            onContentSizeChange={()=>{
              if(this.state.initialLoad){
                this.flatlist.scrollToEnd({animated: false})
              }
            }}
            getItemLayout={(data,index)=>({
              length: 999999999,
              offset: 999999999 * index,
              index 
            })}
            showsVerticalScrollIndicator={false}
            data={this.state.groupedMessages}
            keyExtractor={item => item.chatId}
            CellRendererComponent={({
              children,
              index,
              style,
              item,
              ...props
            }) => {
              if (item.messages.filter( e => e.chatId !== this.state.selectedMessage).length > 0) {
                return (
                  <View style={styles.belowContainer} index={index} {...props}>
                    {children}
                  </View>
                );
              } else {
                return (
                  <View index={index} {...props}>
                    {children}
                  </View>
                );
              }
            }}
            renderItem={({ item }) => (
              <ChatItemsComponent
                {...this.state}
                item={item.messages.map(item => {
                  return item;
                })}
                date={item.date}
                handleToggleDelete={this.handleToggleDelete}
                handleDeleteMessage={this.handleDeleteMessage}
              />
            )}
          />
        </div>

        <div className="send-container">
          {this.state.isImageUpload ?           
            <div className="chat-image-upload-container">
               <ChatImageComponent {...this.state} handleAddImage={this.handleAddImage} handleRemoveImage={this.handleRemoveImage}/>
            </div>
          : null}

          <SendChatComponent 
           {...this.state}
           handleUpdateAddImage={this.handleUpdateAddImage}
           handleInputChange={this.handleInputChange}
           handleSendMessage={this.handleSendMessage}
           />
        </div>
      </div>
    );
  }
}

//Bind dispatch to action creator
//Add Websocket instance
const mapStateToProps = state => {
  return {
    threadMessages: state.chat.threadMessages,
    websocket: state.auth.websocket,
    userId: state.auth.credentials.userId,
    handleName: state.auth.credentials.handleName
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getSpecificChatThread: chatOperations.getSpecificChatThread,
      sendMessage: chatOperations.sendMessage,
      deleteMessage: chatOperations.deleteMessage,
      updateChangeSeenStatus: chatOperations.updateChangeSeenStatus,
      connect: authOperations.connectWS
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ChatConversation);
