import React from "react";
import styles from "./styles/chatconversation.styles.native";
import { View } from "react-native";
import { ListItem } from "react-native-elements";

const ChatHeaderComponent = props => {
  return (
    <View style={styles.listItemContainer}>
      <ListItem
        containerStyle={styles.listItem}
        title={props.recipientHandleName}
        titleStyle={styles.title}
        leftAvatar={{
          source: { uri: "https://www.fillmurray.com/300/100" }
        }}
      />
      <View style={styles.horizontalBorder} />
    </View>
  );
};

export default ChatHeaderComponent;
