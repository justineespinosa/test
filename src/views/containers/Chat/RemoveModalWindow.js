import React from "react"
import {TouchableOpacity, Text, Image} from "react-native"
import styles from "./styles/chatconversation.styles.native"
import DeleteIcon from "../../../includes/img/icons/delete-white.svg";

const RemoveModalWindow = (props) => {
    return(
        <TouchableOpacity
            style={styles.touchableDelete}
            onPress={(e) => {e.preventDefault(); props.handleDeleteMessage(props.item.chatId)}}
        >
            <Image
                source={DeleteIcon}
                alt="delete-icon"
                style={styles.deleteIcon}
            />
            <Text style={styles.deleteText}>削除</Text>
      </TouchableOpacity>
    )
}

export default RemoveModalWindow;