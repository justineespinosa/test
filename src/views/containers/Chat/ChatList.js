import React, { Component } from "react";
import { FlatList, View, TouchableOpacity, Image } from "react-native";
import { ListItem, Button } from "react-native-elements";
import NewChatIcon from "../../../includes/img/icons/edit.svg";
import DeleteIcon from "../../../includes/img/icons/delete.svg";
import MenuIcon from "../../../includes/img/icons/dot-menu.svg";
import MessageIcon from "../../../includes/img/icons/messages.svg";
import { API_ERROR_STATUS, ON_MESSAGE, THREAD_MAX_CHAR, CHAT_TYPE } from "../../../utils/constants";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { chatOperations } from "./state";
import DeleteMenuWindow from "./DeleteMenuWindow";
import RadioButton from "./RadioButton";
import styles from "./styles/chatlist.styles.native.js";
import ConfirmationModal from "../../components/ConfirmationModal/index";
import { isNullOrUndefined } from "util";
import MobileOnlyView from "../../components/MobileOnlyView"
import PCOnlyView from "../../components/PCOnlyView"

class ChatList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: this.props.userId,
      selectedThread: null,
      isModalShow: false,
      isDeleteMenuShow: false,
      isThreadDelete: false,
      currentPagination: 1,
      scrollEnd: true,
      chatThreads: []
    };
  }

  componentDidMount() {
    this.props.getAllChatThread(this.state.userId, this.state.currentPagination)
      .then(response => {
        if (!isNullOrUndefined(response) && API_ERROR_STATUS === response.status ) {
          console.log("error");
        }else{
          this.setState({
            chatThreads : this.props.chatThreads
          })
        }
      });

    if(!isNullOrUndefined(this.ws)){
    this.props.ws.onmessage = (data) => {
        let serverData = JSON.parse(data);
        if(ON_MESSAGE === serverData){
            this.props.getAllChatThread(this.state.userId, this.state.currentPagination)
            .then(response => {
              if (!isNullOrUndefined(response) && API_ERROR_STATUS === response.status ) {
                console.log("error");
              }else{
                this.setState({
                  chatThreads : this.props.chatThreads
                })
              }
            });
        }
      }
    }
  }

  handleChatMessage = n => {
    return THREAD_MAX_CHAR <  n.length  ? `${n.substring(0, THREAD_MAX_CHAR)}...` : n;
  };

  handleCreateMessage = () => {
    this.props.history.push({
      pathname: "/app/newchat"
    });
  };

  handleShowDeleteThread = () => {
    this.setState({
      isDeleteMenuShow: false,
      isThreadDelete: !this.state.isThreadDelete,
      selectedThread: null
    });
  };

  handleDeleteThreadConfirm = () => {
    this.setState({
      isModalShow: !this.state.isModalShow
    });
  };

  handleDeleteThread = () => {
    this.props.deleteChatThread(this.state.selectedThread).then(response => {
      if (API_ERROR_STATUS === response.status) {
        console.log("error");
      } else {
         this.state.chatThreads.map((item, index)=>{
            if(item._id === this.state.selectedThread){
              this.state.chatThreads.splice(index,1)
            }
            return null
          })
          this.setState({
            isThreadDelete: false,
            isModalShow: false,
          });
      }
    });
  };

  handleCancelDeleteThread = () => {
    this.setState({
      isModalShow: !this.state.isModalShow
    });
  };

  handleOpenMessage = item => {
    this.props.history.push({
      pathname: "/app/chatmessages",
      state: {
        friendId: item.recipientId,
        friendHandleName: item.recipientHandleName,
        threadId: item._id
      }
    });
  };

  handleSelectThread = id => {
    this.setState({
      selectedThread: id
    });
  };

  handleToggleDeleteMenu = () => {
    this.setState({
      isDeleteMenuShow: !this.state.isDeleteMenuShow
    });
  };

  handlePagination = () => {
    this.setState({
      currentPagination: this.state.currentPagination + 1
    });

    this.props
      .getAllChatThread(this.state.userId, this.state.currentPagination)
      .then(response => {
        if (API_ERROR_STATUS === response.status) {
          console.log("error");
        }else{
          this.setState({
            chatThreads : [...this.state.chatThreads, ...this.props.chatThreads]
          })
        }
      });
  };

  isAtBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    return layoutMeasurement.height + contentOffset.y >= contentSize.height;
  };

  onScrollEnd = () => {
    this.setState({ scrollEnd: false });
  };

  renderItem = item => {
    return (
        <View style={item.seen ? styles.seenChat : styles.unSeenChat}>
            <ListItem
              title={item.recipientHandleName}
              titleStyle={styles.title}
              subtitle={ item.chat[0].isDeleted ? item.chat[0].senderId === this.state.userId ?
                "メッセージを削除しました。" : item.recipientHandleName + "さんがメッセージを削除しました。"
                : CHAT_TYPE.text === item.chat[0].type ? this.handleChatMessage(item.chat[0].chatBody) 
                : item.chat[0].senderId === this.state.userId ? "写真を送信しました。" : "写真を受信しました。"}
              subtitleStyle={styles.title}
              leftAvatar={{
                size: "xlarge",
                source: { uri: item.url },
                containerStyle: styles.avatar
              }}
              rightElement={
                <RadioButton
                  {...this.state}
                  id={item._id}
                  handleSelectThread={this.handleSelectThread}
                />
              }
              containerStyle={item.seen ? styles.seenChatItem : styles.unseenChatItem}
              onPress={
                !this.state.isThreadDelete ? () => this.handleOpenMessage(item) : () => this.handleSelectThread(item._id)
              }
            />
          </View> 
    );
  };

  renderEmptyChatList = () => {
    return (
      <ListItem
        title="メッセージの会話はまだありません。"
        titleStyle={styles.emptyList}
      />
    );
  };

  renderActionIcons = () => {
    if (this.state.isThreadDelete) {
      return (
        <View style={styles.actionContainer}>
          {null !== this.state.selectedThread ? (
            <TouchableOpacity onPress={() => this.handleDeleteThreadConfirm()}>
              <Image
                alt="delete-icon"
                source={DeleteIcon}
                style={styles.deleteIcon}
              />
            </TouchableOpacity>
          ) : null}
         <PCOnlyView>
            <Button
                title="完了"
                buttonStyle={styles.cancelBtnPC}
                titleStyle={styles.cancelBtnTitle}
                type="outline"
                onPress={() => this.handleShowDeleteThread()}
            />
          </PCOnlyView>
          <MobileOnlyView>
            <Button
                title="完了"
                buttonStyle={styles.cancelBtn}
                titleStyle={styles.cancelBtnTitle}
                type="outline"
                onPress={() => this.handleShowDeleteThread()}
            />
          </MobileOnlyView>
        </View>
      );
    } else {
      return (
        <View style={styles.actionContainer}>
          <TouchableOpacity onPress={() => this.handleToggleDeleteMenu()}>
            <Image alt="pen-icon" source={MenuIcon} style={styles.menuIcon} />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => this.handleCreateMessage()}>
            <Image
              alt="pen-icon"
              source={NewChatIcon}
              style={styles.newChatIcon}
            />
          </TouchableOpacity>
        </View>
      );
    }
  };

  render() {
    return (
      <div className="chat-list-container">
        <View style={styles.listItemContainer}>
            <ListItem
                containerStyle={styles.listItem}
                title="松田直樹"
                titleStyle={styles.title}
                leftAvatar={{
                  source: { uri: "https://www.fillmurray.com/300/100" }
                }}
                rightElement={this.renderActionIcons()}
              />
        </View>
        <View style={styles.horizontalBorder} />
        {this.state.isDeleteMenuShow ? (
          <View style={styles.menuContainer}>
            <TouchableOpacity
              style={styles.menuTouchable}
              onPress={() => this.handleSelectThread()}
            >
              <Image
                alt="pen-icon"
                source={MessageIcon}
                style={styles.messagesIcon}
              />
            </TouchableOpacity>
          </View>
        ) : null}

        <DeleteMenuWindow
          {...this.state}
          handleShowDeleteThread={this.handleShowDeleteThread}
        />

        <div className="flatlist-container">
          <FlatList
            showsVerticalScrollIndicator={false}
            style={styles.listContainer}
            data={this.state.chatThreads}
            keyExtractor={item => item._id.$oid.toString()}
            renderItem={({ item }) => this.renderItem(item)}
            ListEmptyComponent={this.renderEmptyChatList}
            extraData={this.state}
            onScroll={({ nativeEvent }) => {
              if (this.isAtBottom(nativeEvent) && !this.state.scrollEnd) {
                this.handlePagination();
              }
            }}
            bounces={false}
            onEndReached={() => {
              this.onScrollEnd();
            }}
          />
        </div>

        <ConfirmationModal
          {...this.state}
          handleConfirmAction={this.handleDeleteThread}
          handleCloseModal={this.handleCancelDeleteThread}
          customContent={null}
          confirmationText="メッセージを削除しますか？"
        />
      </div>
    );
  }
}

//Bind dispatch to action creator
//Call the websocket instance
const mapStateToProps = state => {
  return {
    chatThreads: state.chat.chatThreads,
    ws: state.auth.websocket,
    userId: state.auth.credentials.userId
  };
};
const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getAllChatThread: chatOperations.getAllChatThread,
      deleteChatThread: chatOperations.deleteChatThread
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(ChatList);
