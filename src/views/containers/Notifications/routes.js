import Notifications from "./Notifications";

export const routes = [
    {
        path: "/notifications",
        component: Notifications,
        exact: true,
        showNav: true
    }
]