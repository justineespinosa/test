import MyPage from "./MyPage";

export const routes = [
    {
        path: "/mypage",
        component: MyPage,
        exact: true,
        showNav: true
    }
]