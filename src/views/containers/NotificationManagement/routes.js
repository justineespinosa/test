import NotificationList from "./NotificationList";

export const routes = [
    {
        path: "/admin-notice",
        component: NotificationList
        ,
        exact: true,
        showNav: true
    }
]