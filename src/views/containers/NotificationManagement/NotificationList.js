import React, { Component } from 'react';
import TableTemplate from '../../components/TableTemplate';
import { notificationsTable, demoForm, selectInput, checkboxInput, radioInput } from './state/templates';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import AdminCard from '../../components/AdminCard';
import FormTemplate from '../../components/FormTemplate';
import { Col } from 'react-bootstrap';
import PaginationTemplate from '../../components/PaginationTemplate';

class NotificationList extends Component {

    state = {
        currentPage: 1,
        totalPage: 5,
    }

    handleSubmit = (values) => {
        alert(`Form Values: ${JSON.stringify(values)}`);
    }

    handlePageChange = (page) => {
        //API Call
        //Update page
        this.setState({ currentPage: page })
    }

    renderPagination = () => {
        return (
            <PaginationTemplate
                currentPage={this.state.currentPage}
                totalPage={this.state.totalPage}
                handlePageChange={(page) => { this.handlePageChange(page) }}
            />
        )
    }

    render() {
        return (
            <React.Fragment>
                <p className="admin-title">お知らせ</p>
                <AdminCard>
                    <p className="admin-subtitle">お知らせ <span className="admin-subtitle-colored">23</span> 件</p>

                    {this.renderPagination()}
                    <TableTemplate
                        tableHeader={notificationsTable.tableHeaders}
                        tableColumns={notificationsTable.tableColumns}
                        tableList={this.props.notifList}
                        rowButtons={[
                            { label: "削除", variant: "link", onClick: () => alert("Delete") },
                        ]}
                    />
                    {this.renderPagination()}
                    
                </AdminCard>

                <Col lg="6" md="12" className="pl-0">
                    <AdminCard>
                        <FormTemplate
                            formInputs={demoForm}
                            selectInput={selectInput}
                            checkboxInput={checkboxInput}
                            radioInput={radioInput}
                            handleSubmit={this.handleSubmit}
                            formButtons={[
                                { type: "primary", label: "Display State", submit: true },
                                { type: "secondary", label: "Dummy Button", onClick: () => alert("Cancel clicked") }
                            ]}
                        />
                    </AdminCard>
                </Col>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        notifList: state.notifmanagement.notificationList
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        //Operations here
    },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(NotificationList);