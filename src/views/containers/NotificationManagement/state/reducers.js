const defaultState = {
    //Module state
    notificationList:[
        {
            id: 1,
            dateCreated: "2020/01/01",
            notifBody: "お知らせタイトルテキストテキストテキス",
            display: "表示",
            notifStartDate: "2020/01/01",
            notifEndDate: "2020/01/04",
        },
        {
            id: 3,
            dateCreated: "2020/01/01",
            notifBody: "お知らせタイトルテキストテキストテキス",
            display: "表示",
            notifStartDate: "2020/01/01",
            notifEndDate: "2020/01/04",
        },
        {
            id: 2,
            dateCreated: "2020/01/01",
            notifBody: "お知らせタイトルテキストテキストテキス",
            display: "表示",
            notifStartDate: "2020/01/01",
            notifEndDate: "2020/01/04",
        }
    ]
};

export default function reducer(state = defaultState, action) {
    switch (action.type) {
        default: return state;
    }
};