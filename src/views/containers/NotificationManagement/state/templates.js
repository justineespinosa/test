export const notificationsTable = {
    tableHeaders: [
        "日付",
        "タイトル",
        "表示/非表示",
        "公開開始日",
        "公開終了日",
        "Actions"
    ],
    tableColumns: [
        { key: "dateCreated", center: true },
        { key: "notifBody", colored: true },
        { key: "display", colored: true, center: true },
        { key: "notifStartDate", center: true },
        { key: "notifEndDate", center: true },
    ]
}


export const demoForm = {
    model: "DemoModel",
    form: [
        {
            type: "text",
            label: "Input #1",
            name: "inputOne",
            default: ''
        },
        {
            type: "multiple-text",
            label: "Multiple",
            children: [
                {
                    type: "custom-text-date",
                    label: "年",
                    name: "year",
                    width: 150,
                    min: 1,
                    max: 9999,
                    default: ''
                },
                {
                    type: "custom-text-date",
                    label: "月",
                    name: "month",
                    width: 85,
                    min: 1,
                    max: 12,
                    default: ''
                },
                {
                    type: "custom-text-date",
                    label: "日",
                    name: "day",
                    width: 85,
                    min: 1,
                    max: 31,
                    default: ''
                },
            ]
        },
        {
            type: "select",
            label: "Select Input",
            name: "inputSelect",
            default: '',
            placeholder: "Select Item . . ."
        },
        {
            type: "datepicker",
            label: "Select Date",
            name: "inputDatepicker"
        },
        {
            type: "multi-checkbox",
            label: "Checkbox",
            name: "multiCheckbox",
            inline: true,
            default: []
        },
        {
            type: "radio",
            label: "Radio Button",
            name: "radioInput",
            inline: true,
            default: ''
        },
    ]
}

export const selectInput = [
    { name: "inputSelect", value: 1, label: "Car" },
    { name: "inputSelect", value: 2, label: "Bag" },
    { name: "inputSelect", value: 3, label: "Pool" }
];

export const checkboxInput = [
    { name: "multiCheckbox", value: 1, label: "Car" },
    { name: "multiCheckbox", value: 2, label: "Bag" },
    { name: "multiCheckbox", value: 3, label: "Pool" },
    { name: "multiInlineCheckbox", value: 1, label: "Dog" },
    { name: "multiInlineCheckbox", value: 2, label: "Cat" },
    { name: "multiInlineCheckbox", value: 3, label: "Fish" }
];

export const radioInput = [
    { name: "radioInput", value: 1, label: "Option #1" },
    { name: "radioInput", value: 2, label: "Option #2" },
    { name: "radioInput", value: 3, label: "Option #3" },
    { name: "radioInlineInput", value: 1, label: "Option #1" },
    { name: "radioInlineInput", value: 2, label: "Option #2" },
    { name: "radioInlineInput", value: 3, label: "Option #3" },
];