import React from 'react';
import { Switch, withRouter, Redirect, Link } from "react-router-dom";
import AppHeader from '../../components/AppHeader';
import { userRoutes } from '../../../app.modules';
import PrivateUserRoute from '../../components/PrivateUserRoute';
import { ROOT, APP_ROOT_ROUTE } from './routes';
import { isMobile } from "react-device-detect";
import { Media } from 'react-breakpoints'
import { Container, Col, Row, Image, Card } from 'react-bootstrap';
import ProfileIcon from '../../../includes/img/user-menu/profile.svg';
import './styles/containers.scss';
import { appendUserRoute } from '../../../utils/common';
import { MenuItem } from '../../components/MenuItem';

export const navList = [
  { navigate: "/newsfeed", label: '最新の更新', icon: require('../../../includes/img/user-menu/newsfeed-dark.svg') },
  { navigate: "/mypage", label: '自分の日記', icon: require('../../../includes/img/user-menu/mypage-dark.svg') },
  { navigate: "/chat", label: 'メッセージ', icon: require('../../../includes/img/user-menu/chat-dark.svg') },
  { navigate: "/notifications", label: 'お知らせ', icon: require('../../../includes/img/user-menu/notification-dark.svg') },
];

export const snsList = [
  { navigate: "/twitter", label: 'Twitter', icon: require('../../../includes/img/user-menu/twitter-dark.svg') },
  { navigate: "/instagram", label: 'Instagram', icon: require('../../../includes/img/user-menu/instagram-dark.svg') },
  { navigate: "/facebook", label: 'Facebook', icon: require('../../../includes/img/user-menu/facebook-dark.svg') },
];

const ModuleContent = (props) => {
  return (
    <Switch>
      {userRoutes.map((route, i) => {
        return (
          <PrivateUserRoute
            {...props.location}
            key={"route" + i}
            path={APP_ROOT_ROUTE + route.path}
            component={route.component}
            exact={route.exact}

          />
        )
      }
      )}
    </Switch>
  )
}

const PCSideNav = () => {
  let appendedRoute = appendUserRoute("/mypage");
  return (
    <div className="user-main-sidenav">
      <Link key={appendedRoute} href={appendedRoute} to={appendedRoute} className="user-main-profile">
        {/*Change to actual handlename*/}
        <Image src={ProfileIcon} style={{ width: 36, height: 36, marginRight: 10 }} roundedCircle />
        <span>{"ハンドルネーム"}</span>
      </Link>

      <Card className="user-main-cardnav">
        {navList.map((item, i) => {
          return <MenuItem key={i} item={item} />
        })}
      </Card>

      <div className="user-main-snsnav">
        {snsList.map((item, i) => {
          return <MenuItem key={i} item={item} />
        })}
      </div>
    </div>
  )
}

const MainContainer = (props) => {
  if (ROOT === props.history.location.pathname) {
    return <Redirect to={{
      pathname: APP_ROOT_ROUTE,
    }} />
  }

  return (
    <React.Fragment>
      <AppHeader />
      <div style={{ marginTop: 93 }}>
        <Media>
          {({ breakpoints, currentBreakpoint }) => {
            let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
            let defaultClass = mobileView ? "m-0 p-0" : "";

            return (
              <Container className={defaultClass}>
                <Row className={mobileView ? "m-0 p-0" : "user-main-container"}>
                  <Col md={mobileView ? "12" : "9"} className={defaultClass}>
                    <ModuleContent {...props} />
                  </Col>
                  {!mobileView && (
                    <Col md="3" sm="12" style={{ cursor: 'default' }}>
                      <PCSideNav {...props} />
                    </Col>
                  )}
                </Row>
              </Container>
            )
          }}
        </Media>

      </div>
    </React.Fragment>
  );
}

export default withRouter(MainContainer);
