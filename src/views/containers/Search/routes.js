import Search from "./Search";

export const routes = [
    {
        path: "/search",
        component: Search,
        exact: true,
        showNav: true
    }
]