import ViewPost from "./ViewPost";

export const routes = [
    {
        path: "/post/view/:id",
        component: ViewPost,
        exact: true,
        showNav: true
    }
]