import React, { Component } from 'react';
import { MODE } from '../../../utils/constants';
import PostViewTemplate from '../../components/PostViewTemplate';
import { NewsfeedList } from '../Top/dummy';

class ViewPost extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mode: MODE.add,
            postId: null,
            type: 0,
            attachmentType: null,
            commentCount: null,
            title: "",
            source: null,
            author: {
                userId: null,
                handleName: "",
                authorDatetime: "",
                profileImageUrl: null,
                isUserDeleted: false,
                isEdited: true
            },
            postBody: "",
            photos:[]
        }
    }

    componentDidMount() {
        let id = this.props.match.params.id;

        if (undefined === id) {
            this.setState({ mode: MODE.add })
        } else {
            //Update to API
            let postDetails = NewsfeedList.filter(item => item.postId === id)[0];

            this.setState({ postId: id, mode: MODE.edit, ...postDetails });
        }
    }

    render() {
        return (
            <PostViewTemplate
                {...this.state}
            />
        );
    }
}

export default ViewPost;