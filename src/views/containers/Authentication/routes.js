import LoginScreen from './LoginScreen'
import UserAuth from './UserAuth'
import MainContainer from "../UserMain/MainContainer";

export const LOGIN_ROUTE = "/admin/login";
export const USER_AUTH_ROUTE = "/user/auth";
export const APP_ROOT_ROUTE = "/app";

export const routes = [
    {
        path: LOGIN_ROUTE,
        component: LoginScreen,
        exact: true,
        public: true
    },
    {
        path: USER_AUTH_ROUTE,
        component: UserAuth,
        exact: true,
        public: true
    },
    {
        path: APP_ROOT_ROUTE,
        component: MainContainer,
        exact: true,
        public: true
    },
];