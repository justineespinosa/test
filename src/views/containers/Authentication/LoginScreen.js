import React, { useState } from "react";
import { authOperations } from './state';
import { Button, FormGroup, FormControl, FormLabel, Container, Card } from "react-bootstrap";
import { Redirect, withRouter } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import './styles/login.scss';
import { ADMIN_ROOT_ROUTE } from "../AdminMain/routes";

const LoginScreen = () => {
  //Hooks state
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");

  //Redux store data
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);

  //Bind dispatch to action creators
  const dispatch = useDispatch();
  const operations = bindActionCreators(
    {
      loginUser: authOperations.loginUser
    },
    dispatch
  )

  const disableForm = () => {
    return 0 >= username.length || 0 >= password.length;
  }


  const handleSubmit = (event) => {
    event.preventDefault();
    operations.loginUser(username, password)
      .then((result) => {
        if (0 >= result.length) {
          setErrorMessage("Incorrect username/password.");
        }
      });
  }

  if (isAuthenticated) {
    return <Redirect to={ADMIN_ROOT_ROUTE + "/user-search"} />;
  }else{
    return (
      <Container className="login-container">
        <Card className="login-card">
          <h5 className="login-title">comucomu 管理画面</h5>
          <h6>{errorMessage}</h6>
  
          <form className="login-form" onSubmit={(e)=>handleSubmit(e)}>
            <FormGroup className="login-input" controlId="email">
              <FormLabel>メールアドレス</FormLabel>
              <FormControl
                size="text"
                autoFocus
                autoComplete="off"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />
            </FormGroup>
  
            <FormGroup className="login-input" controlId="password">
              <FormLabel>パスワード</FormLabel>
              <FormControl
                size="text"
                autoComplete="off"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
  
            <Button
              className="login-button"
              disabled={disableForm()}
              onClick={(e)=>handleSubmit(e)}
              type="submit"
            >
              ログイン
            </Button>
          </form>
        </Card>
      </Container>
    );
  }
}

export default withRouter(LoginScreen);