import React  from "react";
import { authOperations } from './state';
import { Container } from "react-bootstrap";
import { Redirect, withRouter } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { useSelector, useDispatch } from 'react-redux';
import './styles/login.scss';
import { APP_ROOT_ROUTE } from "../UserMain/routes";

const UserAuth = () => {
  //Redux store data
  const isUserAuthenticated = useSelector(state => state.auth.isUserAuthenticated);
  //Bind dispatch to action creators
  const dispatch = useDispatch();
  const operations = bindActionCreators(
    {
      connectWS: authOperations.connectWS
    },
    dispatch
  )

  if(isUserAuthenticated){
    operations.connectWS("wss://ihnj8q7a3g.execute-api.ap-northeast-1.amazonaws.com/api-websocket/")
    return <Redirect to={APP_ROOT_ROUTE} />;
  }else{
    return (
      <Container className="login-container">
      </Container>
    );
  }
}

export default withRouter(UserAuth);