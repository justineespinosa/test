import * as types from "./types";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import { WEBSOCKET_ACTIONS } from "../../../../utils/constants"

const defaultState = {
    isAuthenticated: false,
    isUserAuthenticated: true,
    websocket: null,
    credentials: {
        userId: 1,
        handleName: "Dummy User 1"
    }
};

export default function reducer (state=defaultState, action) {
    switch (action.type) {
        case types.SET_USER_AUTH:
            return {...state, credentials: action.payload, isAuthenticated: action.isAuthenticated};
        case types.SET_WS_CONNECTION:
            let socket = new W3CWebSocket(action.payload)
            socket.onopen = () => {
                socket.send(JSON.stringify({
                    action: WEBSOCKET_ACTIONS.onCreateConnection,
                    data: state.credentials.userId //Assuming data is fetch from current user id
                }))
            }
            return {...state, websocket: socket}
            default: return state;
    }
};