import * as Actions from "./actions";
import clearAuth from "../../../../utils/clearAuth";
// import * as Path from './apiRoutes';
// import ApiService from "../../../../utils/apiService";

const loginUser = (username, password) => (dispatch) => {
    //Test Login
    let testPromise = new Promise((resolve)=>resolve());
    return testPromise.then(() => {
        if ("test" === username && "test" === password) {
            let userData = {
                userId: 2,
                username: "Test",
                firstname: "Dummy",
                lastname: "Account",
                access: "ROLE_ADMIN"
            }
            dispatch(Actions.setUserAuth(userData, true));
            return userData;
        } else {
            return [];
        }
    });

    //Insert Cognito Login Process Here
};

const logoutUser = () => (dispatch) => {
    dispatch(Actions.setUserAuth([], false));
    clearAuth();
};

const connectWS = (ws) => (dispatch) => {
    dispatch(Actions.setWSConnect(ws));
}

export {
    loginUser,
    logoutUser,
    connectWS
};