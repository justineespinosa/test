import React from 'react';
import { Switch, withRouter, Redirect } from "react-router-dom";
import { useSelector } from 'react-redux';
import { LOGIN_ROUTE } from '../Authentication/routes';
import { adminRoutes } from '../../../app.modules';
import { ADMIN_ROOT_ROUTE } from './routes';
import PrivateAdminRoute from '../../components/PrivateAdminRoute';
import AdminHeader from '../../components/AdminHeader';
import AdminSideNav from '../../components/AdminSideNav';
import './styles/container.scss';

const AdminContainer = (props) => {
  //Redux store data
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated);
  const crendentials = useSelector(state => state.auth.crendentials);

  if (!isAuthenticated || (crendentials && "ROLE_ADMIN" === crendentials.access)) {
    return <Redirect to={{
      pathname: LOGIN_ROUTE,
    }} />
  }

  return (
    <div className="admin-container">
      <AdminSideNav/>
      <div className="admin-content">
        <AdminHeader/>
        <div className="admin-body">
          <Switch>
            {adminRoutes.map((route, i) => {
              return (
                <PrivateAdminRoute
                  {...props.location}
                  key={"adminroute" + i}
                  path={ADMIN_ROOT_ROUTE + route.path}
                  component={route.component}
                  exact={route.exact}

                />
              )
            }
            )}
          </Switch>
        </div>
      </div>
    </div>
  );
}

export default withRouter(AdminContainer);
