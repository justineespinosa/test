import React, { useState } from 'react';
import { Card } from 'react-bootstrap';
import AuthorHeader from '../AuthorHeader';
import { Button, Dropdown } from 'react-bootstrap';
import { WRAP_TEXT_CHARS, POST_TYPES, ATTACHMENT_TYPE, UPLOAD_STATUS } from '../../../utils/constants';
import PropTypes from 'prop-types';
import UploadingPlaceholder from '../../../includes/img/placeholders/uploading.svg';
import FailedPlaceholder from '../../../includes/img/placeholders/failed.svg';
import CommentIcon from '../../../includes/img/icons/comments.svg';
import MenuIcon from '../../../includes/img/icons/dot-menu.svg';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";
import { Link, withRouter } from 'react-router-dom';
import { VIMEO_PLAYER_URL } from '../../../config/settings';
import { appendUserRoute } from '../../../utils/common';
import { useSelector } from 'react-redux';
import { MenuItem } from '../MenuItem';
import './styles.scss';
import PhotoGrid from '../PhotoGrid';
import { View } from 'react-native';
import ConfirmationModal from '../ConfirmationModal';

//Custom Component
const ErrorBody = ({ isOwner, attachmentType, photos, videoStatus }) => {
    let hasFailed = false;
    let failedCount = 0;

    switch (attachmentType) {
        case ATTACHMENT_TYPE.image: {
            let counter = photos.filter(item => UPLOAD_STATUS.failed === item.imageStatus).length;
            hasFailed = 0 < counter;
            failedCount = counter;
            break;
        }
        case ATTACHMENT_TYPE.video: {
            hasFailed = UPLOAD_STATUS.failed === videoStatus;
            failedCount = hasFailed ? 1 : 0;
            break;
        }
        default: break;
    }


    return (isOwner && hasFailed) ? <p className="error" style={{ cursor: 'default' }}>{`写真${failedCount}点のアップロードに失敗しました。`}</p> : "";
}

const PostContent = ({ title, postBody, redirectRoute }) => {
    const [showFull, setShowFull] = useState(false);

    if (WRAP_TEXT_CHARS < postBody.length && !showFull) {
        let trimmedText = postBody.substring(0, WRAP_TEXT_CHARS);
        return (
            <span>
                <Link href={redirectRoute} to={redirectRoute} className="post-item-body">
                    <p className="post-title">{title}</p>
                    {trimmedText + " . . ."}
                </Link>
                <Button className="post-body-button" variant="link" onClick={() => setShowFull(true)}>もっと見る</Button>
            </span>
        )
    } else {
        return <Link href={redirectRoute} to={redirectRoute} className="post-item-body"><p><b>{title}</b></p>{postBody}</Link>;
    }
}

const CommentCount = ({ count, redirectRoute }) => {
    return (
        <div className="post-item-container" style={{ paddingTop: 0 }}>
            <div className="post-item-comment">
                <Link href={redirectRoute} to={redirectRoute} className="post-item-comment-label">
                    <span>
                        <img className="post-item-comment-icon" src={CommentIcon} alt="comment-icon" />
                        <span className="post-item-comment-label">{"コメントする"}</span>
                    </span>
                </Link>
                {0 < count && <Link href={redirectRoute} to={redirectRoute} className="post-item-comment-label">{`コメント${count}件`}</Link>}
            </div>
        </div>
    );
}

//Functions
const RenderLivestream = ({ vimeoLivestreamId }) => {
    return (
        <div className="post-item-video-container">
            <iframe
                title={vimeoLivestreamId}
                src={`${VIMEO_PLAYER_URL}/video/${vimeoLivestreamId}?title=0&byline=0&portrait=0`}
                frameBorder="0"
                allow="autoplay; fullscreen"
                allowFullScreen
                className="post-item-video-content"
            >
            </iframe>
        </div>
    )
}

const RenderVideo = ({ videoUrl, videoStatus, isOwner, onRedirect }) => {
    switch (videoStatus) {
        case UPLOAD_STATUS.uploading: return <img alt="uploading" onClick={onRedirect} className="post-item-placeholder-video" src={UploadingPlaceholder} />;
        case UPLOAD_STATUS.failed: return isOwner ? <img alt="failed attachment" onClick={onRedirect} className="post-item-placeholder-video" src={FailedPlaceholder} /> : '';
        case UPLOAD_STATUS.success:
        default:
            return (
                <div className="post-item-video-container">
                    <iframe
                        title={videoUrl}
                        src={`${VIMEO_PLAYER_URL + videoUrl}?title=0&byline=0&portrait=0`}
                        frameBorder="0"
                        allow="autoplay; fullscreen"
                        allowFullScreen
                        className="post-item-video-content"
                    >
                    </iframe>
                </div>
            );
    }
}

const RenderPhotos = ({ photos, isOwner, onRedirect }) => {
    let previewPhotos = isOwner ? photos : photos.filter(item => UPLOAD_STATUS.failed !== item.imageStatus);
    let mediaUri = previewPhotos.map(item => {
        switch (item.imageStatus) {
            case UPLOAD_STATUS.uploading: return UploadingPlaceholder;
            case UPLOAD_STATUS.success: return item.imageUrl;
            case UPLOAD_STATUS.failed: return FailedPlaceholder;
            default: return item.imageUrl;
        }
    });
    return (
        <View style={{ flex: 1 }}>
            <PhotoGrid
                source={mediaUri}
                onPressImage={onRedirect}
            />
        </View>
    )
}

const CustomToggle = React.forwardRef(({ onClick }, ref) => (
    <a
        href="_blank"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        <img alt="menu-icon" src={MenuIcon} style={{ width: 24, height: 24 }} />
    </a>
));

const PostItemTemplate = (props) => {
    let appendedPostViewRoute = appendUserRoute(`/post/view/${props.postId}`);
    let appendedCommunityPostViewRoute = appendUserRoute(`/communities/${props.communityId}/post/${props.postId}`);
    let redirectRoute = props.isCommunity ? appendedCommunityPostViewRoute : appendedPostViewRoute;

    //Hooks state
    const [showDelete, setShowDelete] = useState(false);

    //Redirect View Method
    const redirectViewPost = () => props.history.push(redirectRoute);

    //Redirect Edit Method
    const redirectEditPost = () => props.history.push(appendUserRoute(`/post/details/${props.postId}`));

    //Redux store data
    const userId = useSelector(state => state.auth.credentials.userId);

    //Conditions
    const isOwner = userId === props.author.userId;
    const displayLiveStream = POST_TYPES.livestream === props.type;
    const displayPhotos = (ATTACHMENT_TYPE.image === props.attachmentType && 0 < props.photos.filter(item => UPLOAD_STATUS.failed !== item.imageStatus).length);
    const displayVideo = ATTACHMENT_TYPE.video === props.attachmentType;

    //Menu Items
    const ownerMenu = [
        { item: { label: "日記を編集", icon: require('../../../includes/img/icons/edit-white.svg') }, onPress: redirectEditPost },
        { item: { label: "写真を追加", icon: require('../../../includes/img/icons/camera-white.svg') }, onPress: redirectEditPost },
        { item: { label: "日記を削除", icon: require('../../../includes/img/icons/delete-white.svg') }, onPress: () => setShowDelete(true) }
    ]
    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                return (
                    <React.Fragment>
                        <Card className="feed-container-item" style={mobileView ? { border: 'none' } : {}}>
                            <Card.Body style={{ padding: 0 }}>
                                <div className="post-item-container">
                                    <AuthorHeader
                                        {...props.author}
                                        createDt={props.createDt}
                                        updateDt={props.updateDt}
                                    />

                                    {
                                        isOwner && (
                                            <Dropdown className="post-item-menu">
                                                <Dropdown.Toggle as={CustomToggle} id="dropdown-post-menu" />
                                                <Dropdown.Menu alignRight className="post-item-menu-dropdown">
                                                    {ownerMenu.map((menu, i) => (
                                                        <MenuItem
                                                            key={"user-menu" + i}
                                                            action={true}
                                                            item={menu.item}
                                                            onPress={menu.onPress}
                                                            titleStyle={{ color: "#fff", marginLeft: 12 }}
                                                        />
                                                    )
                                                    )}
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        )
                                    }

                                    <ErrorBody
                                        isOwner={isOwner}
                                        photos={props.photos}
                                        attachmentType={props.attachmentType}
                                        videoStatus={props.videoStatus}
                                    />
                                    <PostContent title={props.title} postBody={props.postBody} redirectRoute={redirectRoute} />
                                </div>

                                {displayLiveStream && (
                                    <RenderLivestream vimeoLivestreamId={props.vimeoLivestreamId} />
                                )}

                                {displayPhotos && (
                                    <RenderPhotos
                                        photos={props.photos}
                                        isOwner={isOwner}
                                        onRedirect={redirectViewPost}
                                    />
                                )}

                                {displayVideo && (
                                    <RenderVideo
                                        videoUrl={props.videoUrl}
                                        videoStatus={props.videoStatus}
                                        isOwner={isOwner}
                                        onRedirect={redirectViewPost}
                                    />
                                )}

                                <CommentCount count={props.commentCount} redirectRoute={redirectRoute} />
                            </Card.Body>
                        </Card>

                        {/* Delete PostConfirmation */}
                        <ConfirmationModal
                            isModalShow={showDelete}
                            confirmationText={"日記を削除しますか？"}
                            handleConfirmAction={() => alert("Post Delete")}
                            handleCloseModal={() => setShowDelete(false)}
                        />
                    </React.Fragment>
                )
            }}
        </Media>
    );
};

export default withRouter(PostItemTemplate);

PostItemTemplate.propTypes = {
    userId: PropTypes.number,
    deletePost: PropTypes.func,
    isCommunity: PropTypes.bool,
    communityId: PropTypes.string,
    postId: PropTypes.string,
    author: PropTypes.object,
    createDt: PropTypes.string,
    updateDt: PropTypes.string,
    title: PropTypes.string,
    postBody: PropTypes.string,
    type: PropTypes.number,
    attachmentType: PropTypes.number,
    photos: PropTypes.array,
    videoURL: PropTypes.string,
    videoStatus: PropTypes.number,
    commentCount: PropTypes.number,
    source: PropTypes.number,
    vimeoLivestreamId: PropTypes.string,
    startLivestreamDt: PropTypes.string,
    endLivestreamDt: PropTypes.string,
    isPoll: PropTypes.bool,
    poll: PropTypes.array,
};