import React from "react";
import Modal from "react-modal";
import { View, Text, Image, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import "./styles.scss";
import CloseIcon from "../../../includes/img/icons/close.svg";
import styles from "./styles.native.js";

const ConfirmationModal = props => {
  return (
    <View>
      <Modal
        ariaHideApp={false}
        isOpen={props.isModalShow}
        className="delete-modal"
        overlayClassName="delete-modal-overlay"
      >
        <TouchableOpacity
          style={styles.modalClose}
          onPress={() => props.handleCloseModal()}
        >
          <Image source={CloseIcon} alt="close-icon" style={styles.closeIcon} />
        </TouchableOpacity>

        <View>
          <Text style={styles.modalText}>{props.confirmationText}</Text>
        </View>
        <div className="btn-container">
          <Button
            onClick={() => props.handleConfirmAction()}
            buttonStyle={styles.confirmBtn}
            title={props.confirmTitle || "はい"}
            titleStyle={styles.titleConfirm}
          />
          <Button
            onClick={() => props.handleCloseModal()}
            buttonStyle={styles.closeBtn}
            title={props.closeTitle || "戻る"}
            titleStyle={styles.titleClose}
          />
        </div>
      </Modal>
    </View>
  );
};

export default ConfirmationModal;
