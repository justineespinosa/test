import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  modalClose: {
    position: "absolute",
    top: "-5px",
    right: "-5px",
    zIndex: 2
  },
  closeIcon: {
    width: 22,
    height: 22
  },
  modalText: {
    marginTop: "50px",
    marginBottom: "30px",
    textAlign: "center"
  },
  confirmBtn: {
    backgroundColor: "#0a182c",
    width: 200,
    marginRight: 20,
    marginBottom: 10,
    borderRadius: 10,
    height: 40
  },
  closeBtn: {
    backgroundColor: "#dcdcdc",
    width: 200,
    borderRadius: 10,
    height: 40
  },
  titleConfirm: {
    color: "#ffffff",
    fontSize: 12
  },
  titleClose: {
    color: "#666666",
    fontSize: 12
  }
});

export default styles;
