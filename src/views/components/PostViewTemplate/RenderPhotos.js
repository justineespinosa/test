import React from 'react';
import { Image } from 'react-bootstrap';
import { UPLOAD_STATUS } from '../../../utils/constants';
import UploadingPlaceholder from '../../../includes/img/placeholders/uploading.svg';
import FailedPlaceholder from '../../../includes/img/placeholders/failed.svg';
import RenderCommentCount from './RenderCommentCount';
import './styles.scss';
import PostContent from './PostContent';
import RenderComments from './RenderComments';
import './styles.scss';

const RenderPhotos = ({ photos, userId, author, source, isAlbum }) => {
    let isOwner = userId === author.userId;

    const renderItem = (uri, item, index) => {
        if (isAlbum) {
            return (
                <div key={"i" + index} className="post-view-album-image">
                    <div className="post-view-container">
                        <PostContent title={item.title || ''} postBody={item.body || ''} />
                    </div>
                    <Image src={uri} className="post-view-image" alt="post-image" style={{ marginBottom: -1 }} />
                    <RenderCommentCount count={item.comments ? item.comments.length : 0} />
                    <RenderComments
                        props={source}
                        comments={item.comments}
                        userId={userId}
                    />
                </div>
            )
        } else {
            return <Image key={"i" + index} src={uri} className="post-view-image" alt="post-image" />
        }
    }

    return photos.map((item, i) => {
        switch (item.imageStatus) {
            case UPLOAD_STATUS.uploading: return renderItem(UploadingPlaceholder, item, i);
            case UPLOAD_STATUS.success: return renderItem(item.imageUrl, item, i);
            case UPLOAD_STATUS.failed: return isOwner ? renderItem(FailedPlaceholder, item, i) : null;
            default: return renderItem(item.imageUrl);
        }
    });
}


export default RenderPhotos;