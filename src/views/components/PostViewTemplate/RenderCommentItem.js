import React from 'react';
import { Image, Media as BootstrapMedia } from 'react-bootstrap';
import DefaultIcon from '../../../includes/img/placeholders/user.svg';
import ResponseArea from './ResponseArea';
import RenderReply from './RenderReply';
import { appendUserRoute } from '../../../utils/common';
import { Link } from 'react-router-dom';
import './styles.scss';

const RenderCommentItem = (props) => {
    let isOwner = props.userId === props.author.userId;

    //User Route
    let appendedRoute = appendUserRoute(`/user/${props.author.userId}`);
    
    if (isOwner) {
        appendedRoute = appendUserRoute("/myPage");
    }

    return (
        <BootstrapMedia>
            <Link href={appendedRoute} to={appendedRoute}>
                <Image
                    src={props.author.profileImageUrl || DefaultIcon}
                    className="post-view-avatar"
                    roundedCircle
                />
            </Link>
            <BootstrapMedia.Body>
                <ResponseArea
                    commentId={props._id}
                    postId={props.postId}
                    type={"comment"}
                    replyCount={props.replies ? props.replies.length : 0}
                    value={props.commentBody}
                    author={props.author}
                    createDt={props.createDt}
                    updateDt={props.updateDt}
                    source={props.source}
                    isOwner={isOwner}
                />
                {props.replies && props.replies.map((reply, i) => {
                    return (
                        <RenderReply
                            {...reply}
                            key={"reply" + i}
                            postId={props.postId}
                            source={props.source}
                            userId={props.userId}
                        />);
                })}
            </BootstrapMedia.Body>
        </BootstrapMedia>
    )
}

export default RenderCommentItem;