import React from 'react';
import { Image,Media as BootstrapMedia } from 'react-bootstrap';
import DefaultIcon from '../../../includes/img/placeholders/user.svg';
import ResponseArea from './ResponseArea';
import { Link } from 'react-router-dom';
import { appendUserRoute } from '../../../utils/common';
import './styles.scss';

const RenderReply = (props) => {
    let isOwner = props.userId === props.author.userId;

    //User Route
    let appendedRoute = appendUserRoute(`/user/${props.author.userId}`);
    
    if (isOwner) {
        appendedRoute = appendUserRoute("/myPage");
    }

    return (
        <BootstrapMedia style={{ marginLeft: 12 }}>
            <Link href={appendedRoute} to={appendedRoute}>
                <Image
                    src={props.author.profileImageUrl || DefaultIcon}
                    className="post-view-avatar"
                    roundedCircle
                />
            </Link>
            <BootstrapMedia.Body>
                <ResponseArea
                    type={"reply"}
                    value={props.replyBody}
                    replyCount={0}
                    author={props.author}
                    createDt={props.createDt}
                    updateDt={props.updateDt}
                    source={props.source}
                    isOwner={isOwner}
                />
            </BootstrapMedia.Body>
        </BootstrapMedia>
    )
}

export default RenderReply;