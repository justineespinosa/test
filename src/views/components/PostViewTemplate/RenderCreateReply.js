import React, { useState } from 'react';
import { Image, Media as BootstrapMedia, Col } from 'react-bootstrap';
import DefaultIcon from '../../../includes/img/placeholders/user.svg';
import { useSelector } from 'react-redux';
import TextareaAutosize from '../../../../node_modules/react-autosize-textarea/lib';
import SaveIcon from '../../../includes/img/icons/save.svg';
import SendWhiteIcon from '../../../includes/img/icons/send-white.svg';
import { REPLIES_MAX_CHAR } from '../../../utils/constants';
import './styles.scss';
import PCOnlyButton from '../PCOnlyButton';
import { emptyTextValue } from '../../../utils/common';
import MobileOnlyButton from '../MobileOnlyButton';

const RenderCreateReply = ({ commentId, mobileView }) => {
    //Redux store data
    const user = useSelector(state => state.auth.credentials);

    //Hooks state
    const [reply, setReply] = useState("");

    return (
        <BootstrapMedia style={{ marginLeft: 12 }}>
            <Image
                src={user.profileImageUrl || DefaultIcon}
                className="post-view-avatar"
                roundedCircle
            />
            <BootstrapMedia.Body>
                <Col style={{ marginBottom: 14, paddingRight: 0 }}>
                    <div className="d-flex flex-direction-row">
                        <TextareaAutosize
                            value={reply}
                            maxLength={REPLIES_MAX_CHAR}
                            onChange={e => setReply(e.target.value)}
                            placeholder={"返信する"}
                            className={`form-control post-view-comment-textarea-${mobileView ? "mobile" : "pc"}`}
                            style={{ marginLeft: 0 }}
                        />

                        <MobileOnlyButton
                            disabled={emptyTextValue(reply)}
                            onPress={() => alert(`Create Reply for ${commentId}: ${reply}` )}
                            buttonStyle={{ alignItems: 'center', marginLeft: 12 }}
                            labelStyle={{fontSize: 14}}
                            icon={SaveIcon}
                            label={'保存'}
                        />
                    </div>

                    <span style={{ paddingRight: mobileView ? 32 : 0 }} className={`d-flex justify-content-end mt-2 mb-4`}>
                        <PCOnlyButton
                            disabled={emptyTextValue(reply)}
                            label={'返信する'}
                            icon={SendWhiteIcon}
                            onClick={() => alert("Create Reply")}
                        />
                    </span>
                </Col>
            </BootstrapMedia.Body>
        </BootstrapMedia>
    )
}

export default RenderCreateReply;