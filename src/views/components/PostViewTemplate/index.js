import React, { useState, useEffect } from 'react';
import AuthorHeader from '../AuthorHeader';
import { Dropdown, Card } from 'react-bootstrap';
import { POST_TYPES, ATTACHMENT_TYPE, UPLOAD_STATUS, SOURCE } from '../../../utils/constants';
import PropTypes from 'prop-types';
import MenuIcon from '../../../includes/img/icons/dot-menu.svg';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";
import { withRouter } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { appendUserRoute } from '../../../utils/common';
import { MenuItem } from '../MenuItem';
import './styles.scss';
import RenderCreateComment from './RenderCreateComment';
import RenderCommentCount from './RenderCommentCount';
import RenderLivestream from './RenderLivestream';
import RenderVideo from './RenderVideo';
import RenderPhotos from './RenderPhotos';
import PostContent from './PostContent';
import RenderComments from './RenderComments';
import ConfirmationModal from '../ConfirmationModal';

//Custom Component
const ErrorBody = ({ userId, authorId, attachmentType, photos, videoStatus }) => {
    let hasFailed = false;
    let failedCount = 0;

    switch (attachmentType) {
        case ATTACHMENT_TYPE.image: {
            let counter = photos.filter(item => UPLOAD_STATUS.failed === item.imageStatus).length;
            hasFailed = 0 < counter;
            failedCount = counter;
            break;
        }
        case ATTACHMENT_TYPE.video: {
            hasFailed = UPLOAD_STATUS.failed === videoStatus;
            failedCount = hasFailed ? 1 : 0;
            break;
        }
        default: break;
    }


    return (userId === authorId && hasFailed) ? <p className="error" style={{ cursor: 'default' }}>{`写真${failedCount}点のアップロードに失敗しました。`}</p> : "";
}

const CustomToggle = React.forwardRef(({ onClick }, ref) => (
    <a
        href="_blank"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        <img alt="menu-icon" src={MenuIcon} style={{ width: 24, height: 24 }} />
    </a>
));

const PostViewTemplate = (props) => {
    //Redux store data
    const userId = useSelector(state => state.auth.credentials.userId);

    //Hooks state
    const [showDelete, setShowDelete] = useState(false);

    //Default top screen
    useEffect(() => { window.scrollTo(0, 0); }, [])

    //Redirect Edit Method
    const redirectEditPost = () => props.history.push(appendUserRoute(`/post/details/${props.postId}`));

    //Conditions
    const isOwner = userId === props.author.userId;
    const displayLiveStream = POST_TYPES.livestream === props.type;
    const displayPhotos = (POST_TYPES.normal === props.type && ATTACHMENT_TYPE.image === props.attachmentType && 0 < props.photos.filter(item => UPLOAD_STATUS.failed !== item.imageStatus).length);
    const displayVideo = ATTACHMENT_TYPE.video === props.attachmentType;
    const displayCreateComment = (POST_TYPES.album !== props.type && SOURCE.new === props.source);
    const displayAlbum = POST_TYPES.album === props.type;

    //Menu Items
    const ownerMenu = [
        { item: { label: "日記を編集", icon: require('../../../includes/img/icons/edit-white.svg') }, onPress: redirectEditPost },
        { item: { label: "写真を追加", icon: require('../../../includes/img/icons/camera-white.svg') }, onPress: redirectEditPost },
        { item: { label: "日記を削除", icon: require('../../../includes/img/icons/delete-white.svg') }, onPress: () => setShowDelete(true) }
    ]

    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                return (
                    <React.Fragment>
                        <Card className="feed-container-item" style={mobileView ? { border: 'none' } : {}}>
                            <Card.Body style={{ padding: 0 }}>
                                <div className="post-view-container">
                                    <AuthorHeader
                                        {...props.author}
                                        createDt={props.createDt}
                                        updateDt={props.updateDt}
                                    />

                                    {
                                        isOwner && (
                                            <Dropdown className="post-item-menu">
                                                <Dropdown.Toggle as={CustomToggle} id="dropdown-post-menu" />
                                                <Dropdown.Menu alignRight className="post-item-menu-dropdown" >
                                                    {ownerMenu.map((menu, i) => (
                                                        <MenuItem
                                                            key={"menu" + i}
                                                            action={true}
                                                            item={menu.item}
                                                            onPress={menu.onPress}
                                                            titleStyle={{ color: "#fff", marginLeft: 12 }}
                                                        />
                                                    )
                                                    )}
                                                </Dropdown.Menu>
                                            </Dropdown>
                                        )
                                    }

                                    <ErrorBody
                                        userId={userId}
                                        authorId={props.author.userId}
                                        photos={props.photos}
                                        attachmentType={props.attachmentType}
                                        videoStatus={props.videoStatus}
                                    />
                                    <PostContent title={props.title} postBody={props.postBody} />
                                </div>

                                {displayLiveStream && (
                                    <RenderLivestream vimeoLivestreamId={props.vimeoLivestreamId} />
                                )}

                                {displayPhotos && (
                                    <div>
                                        <RenderPhotos
                                            {...props}
                                            userId={userId}
                                        />
                                    </div>
                                )}

                                {displayVideo && (
                                    <RenderVideo
                                        {...props}
                                        userId={userId}
                                    />
                                )}

                                <RenderCommentCount count={props.commentCount} />

                                {displayCreateComment && (
                                    <RenderCreateComment />
                                )}

                                <RenderComments
                                    postId={props.postId}
                                    source={props.source}
                                    comments={props.comments}
                                    userId={userId}
                                />

                                {displayAlbum && (
                                    <RenderPhotos
                                        {...props}
                                        isAlbum={true}
                                        userId={userId}
                                    />
                                )}
                            </Card.Body>
                        </Card>

                        {/* Delete PostConfirmation */}
                        <ConfirmationModal
                            isModalShow={showDelete}
                            confirmationText={"日記を削除しますか？"}
                            handleConfirmAction={() => alert("Post Delete")}
                            handleCloseModal={() => setShowDelete(false)}
                        />

                    </React.Fragment>
                )
            }}
        </Media>
    );
};

export default withRouter(PostViewTemplate);

PostViewTemplate.propTypes = {
    userId: PropTypes.number,
    deletePost: PropTypes.func,
    isCommunity: PropTypes.bool,
    communityId: PropTypes.string,
    postId: PropTypes.string,
    author: PropTypes.object,
    createDt: PropTypes.string,
    updateDt: PropTypes.string,
    title: PropTypes.string,
    postBody: PropTypes.string,
    type: PropTypes.number,
    attachmentType: PropTypes.number,
    photos: PropTypes.array,
    videoURL: PropTypes.string,
    videoStatus: PropTypes.number,
    commentCount: PropTypes.number,
    source: PropTypes.number,
    vimeoLivestreamId: PropTypes.string,
    startLivestreamDt: PropTypes.string,
    endLivestreamDt: PropTypes.string,
    isPoll: PropTypes.bool,
    poll: PropTypes.array,
};