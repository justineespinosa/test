import React, { useState } from 'react';
import { Image, Container } from 'react-bootstrap';
import { COMMENTS_MAX_CHAR } from '../../../utils/constants';
import DefaultIcon from '../../../includes/img/placeholders/user.svg';
import SendIcon from '../../../includes/img/icons/send.svg';
import SendWhiteIcon from '../../../includes/img/icons/send-white.svg';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";
import { useSelector } from 'react-redux';
import TextareaAutosize from 'react-autosize-textarea';
import './styles.scss';
import PCOnlyButton from '../PCOnlyButton';
import { emptyTextValue } from '../../../utils/common';
import MobileOnlyButton from '../MobileOnlyButton';

const RenderCreateComment = () => {
    const [txtComment, setTxtComment] = useState("");

    //Redux store data
    const loggedUser = useSelector(state => state.auth.credentials);

    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                return (
                    <Container className={`post-view-comment-create-${mobileView ? "mobile" : "pc"}`}>
                        <div className="post-view-comment-body">
                            <Image
                                src={loggedUser.profileImageUrl || DefaultIcon}
                                className="post-view-avatar"
                                roundedCircle
                            />
                            <TextareaAutosize
                                value={txtComment}
                                maxLength={COMMENTS_MAX_CHAR}
                                onChange={e => setTxtComment(e.target.value)}
                                rows={3}
                                placeholder={"コメントを投稿する"}
                                className={`form-control post-view-comment-textarea-${mobileView ? "mobile" : "pc"}`}
                            />
                        </div>

                        <div className={`d-flex justify-content-end ml-2 ${!mobileView && 'mt-3'}`}>
                            <PCOnlyButton
                                disabled={emptyTextValue(txtComment)}
                                label={'投稿する'}
                                icon={SendWhiteIcon}
                                onClick={() => alert("create comment")}
                            />

                            <MobileOnlyButton
                                disabled={emptyTextValue(txtComment)}
                                onPress={() => alert(`Create Comment: ${txtComment}`)}
                                buttonStyle={{ alignItems: 'center' }}
                                labelStyle={{fontSize: 14}}
                                icon={SendIcon}
                                label={'投稿'}
                            />
                        </div>
                    </Container>
                );
            }}
        </Media>
    )
}

export default RenderCreateComment;