import React, { useState } from 'react';
import { Col, Row, Button } from 'react-bootstrap';
import { TouchableOpacity, Text } from 'react-native';
import { DATETIME_FORMAT, SOURCE, COMMENTS_MAX_CHAR, REPLIES_MAX_CHAR, WRAP_TEXT_CHARS } from '../../../utils/constants';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";
import moment from 'moment';
import TextareaAutosize from 'react-autosize-textarea';
import SaveIcon from '../../../includes/img/icons/save.svg';
import SaveWhiteIcon from '../../../includes/img/icons/save-white.svg';
import './styles.scss';
import ConfirmationModal from '../ConfirmationModal';
import { isFumiyaUserCheck, emptyTextValue, appendUserRoute } from '../../../utils/common';
import RenderCreateReply from './RenderCreateReply';
import PCOnlyButton from '../PCOnlyButton';
import MobileOnlyButton from '../MobileOnlyButton';
import { Link } from 'react-router-dom';

const HandleName = (props) => {
    let { appendedRoute, isUserDeleted, handleName } = props;
    if (isUserDeleted) {
        return (
            <span className="post-view-handlename">{"退会ユーザ"}</span>
        )
    } else {
        return (
            <Link href={appendedRoute} to={appendedRoute} className="post-view-handlename">
                <span>{handleName}</span>
            </Link>
        )
    }
}

const ResponseButtons = (props) => {
    let buttonStyle = { marginLeft: 20, paddingVertical: props.editMode ? 0 : 4, paddingHorizontal: 4 };

    if (props.isComment) {
        return (
            <div className="post-view-comment-buttons">
                {props.isOwner && (
                    <React.Fragment>
                        {!props.editMode && <TouchableOpacity style={buttonStyle} onPress={() => props.setEditMode(true)}><Text>{'編集'}</Text></TouchableOpacity>}
                        <TouchableOpacity style={buttonStyle} onPress={() => props.setShowDelete(true)}><Text>{'削除'}</Text></TouchableOpacity>
                    </React.Fragment>
                )}
                {props.editMode ? (
                    <React.Fragment>
                        <TouchableOpacity
                            style={buttonStyle}
                            onPress={() => props.setShowDiscard(true)}
                        >
                            <Text>{'キャンセル'}</Text>
                        </TouchableOpacity>

                        <PCOnlyButton
                            label={'保存する'}
                            icon={SaveWhiteIcon}
                            onClick={() => alert("Save Comment")}
                        />
                    </React.Fragment>
                ) : (
                        <TouchableOpacity style={buttonStyle} onPress={() => props.setShowCreateReply(true)}><Text>{'返信'}</Text></TouchableOpacity>
                    )}
            </div>
        )
    } else {
        if (props.isOwner) {
            return (
                <div className="post-view-comment-buttons">
                    {!props.editMode && <TouchableOpacity style={buttonStyle} onPress={() => props.setEditMode(true)}><Text>{'編集'}</Text></TouchableOpacity>}

                    <TouchableOpacity style={buttonStyle} onPress={() => props.setShowDelete(true)}><Text>{'削除'}</Text></TouchableOpacity>

                    {props.editMode && (
                        <React.Fragment>
                            <TouchableOpacity
                                style={buttonStyle}
                                onPress={() => props.setShowDiscard(true)}
                            >
                                <Text>{'キャンセル '}</Text>
                            </TouchableOpacity>

                            <PCOnlyButton
                                label={'保存する'}
                                icon={SaveWhiteIcon}
                                onClick={() => alert("Save Reply")}
                            />
                        </React.Fragment>
                    )}
                </div>
            )
        }
    }

    return '';
}

const ResponseArea = (props) => {
    let { type, value, author, source, createDt, updateDt, isOwner } = props;
    let isComment = "comment" === type;
    let textPreview = value.substring(0, WRAP_TEXT_CHARS);

    const [responseValue, setResponseValue] = useState(value);
    const [editMode, setEditMode] = useState(false);
    const [showDiscard, setShowDiscard] = useState(false);
    const [showDelete, setShowDelete] = useState(false);
    const [showCreateReply, setShowCreateReply] = useState(false);
    const [showFull, setShowFull] = useState(false);

    let isTextWrapped = WRAP_TEXT_CHARS < value.length && !showFull;

    //User Route
    let appendedRoute = appendUserRoute(`/user/${author.userId}`);
    if (isOwner) {
        appendedRoute = appendUserRoute("/myPage");
    }

    let msgDeleteComment = "コメントを削除しますか？";

    if (isFumiyaUserCheck(props.author.userId)) {
        msgDeleteComment = 'フミヤさんの日記には1ユーザあたり1件しかコメントを投稿することができません。\nこのコメントを削除しても、この日記に新たにコメントを投稿することはできません。\nのコメントを削除しますか？';
    } else if (0 < props.replyCount) {
        msgDeleteComment = 'このコメントを削除すると、返信コメントも合わせて削除されます。\nこのコメントを削除しますか？';
    }

    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                return (
                    <React.Fragment>
                        <Row className="m-0 p-0">
                            <Col style={{ marginBottom: 14, paddingRight: 0 }}>
                                {editMode ? (
                                    <React.Fragment>
                                        <p className="post-view-handlename">{author.handleName}</p>
                                        <div className="d-flex flex-direction-row">
                                            <TextareaAutosize
                                                value={responseValue}
                                                maxLength={isComment ? COMMENTS_MAX_CHAR : REPLIES_MAX_CHAR}
                                                onChange={e => setResponseValue(e.target.value)}
                                                placeholder={isComment ? "コメント" : "返信する"}
                                                className={`form-control post-view-comment-textarea-${mobileView ? "mobile" : "pc"}`}
                                                style={{ marginLeft: 0 }}
                                            />

                                            {editMode && (
                                                <MobileOnlyButton
                                                    disabled={emptyTextValue(responseValue)}
                                                    onPress={() => {
                                                        if (isComment) {
                                                            alert(`Save Comment: ${responseValue}`)
                                                        } else {
                                                            alert(`Save Reply: ${responseValue}`)
                                                        }
                                                    }}
                                                    buttonStyle={{ alignItems: 'center', marginLeft: 12 }}
                                                    labelStyle={{ fontSize: 14 }}
                                                    icon={SaveIcon}
                                                    label={'保存'}
                                                />
                                            )}
                                        </div>
                                    </React.Fragment>
                                ) : (
                                        <div className={`post-view-readonly-input-${mobileView ? "mobile" : "pc"}`}>
                                            <HandleName
                                                appendedRoute={appendedRoute}
                                                isUserDeleted={author.isUserDeleted}
                                                handleName={author.handleName}
                                            />
                                            <br />
                                            
                                            {isTextWrapped ? (
                                                <span>
                                                    {textPreview + " . . ."}
                                                    <Button className="post-body-button" variant="link" onClick={() => setShowFull(true)}>もっと見る</Button>
                                                </span>
                                            ) : value}
                                        </div>
                                    )
                                }


                                <span style={{ paddingRight: editMode && mobileView ? 32 : 0 }} className={`post-view-comment-below${editMode ? "-edit" : ''}`}>
                                    <div className="post-view-datetime">
                                        {moment(new Date(createDt)).format(DATETIME_FORMAT)}
                                        {moment(new Date(updateDt)).isValid() && " | 編集済"}
                                    </div>
                                    {SOURCE.new === source && (
                                        <ResponseButtons
                                            {...props}
                                            isComment={isComment}
                                            editMode={editMode}
                                            setEditMode={setEditMode}
                                            responseValue={responseValue}
                                            setResponseValue={setResponseValue}
                                            showDiscard={showDiscard}
                                            setShowDiscard={setShowDiscard}
                                            showDelete={showDelete}
                                            setShowDelete={setShowDelete}
                                            showCreateReply={showCreateReply}
                                            setShowCreateReply={setShowCreateReply}
                                        />
                                    )}
                                </span>
                            </Col>
                        </Row>

                        {/* Discard Confirmation */}
                        <ConfirmationModal
                            isModalShow={showDiscard}
                            confirmTitle={"OK"}
                            confirmationText={"更新された内容が破棄されます。\nよろしいですか？"}
                            handleConfirmAction={() => {
                                setResponseValue(props.value)
                                setEditMode(false)
                                setShowDiscard(false)
                            }}
                            handleCloseModal={() => setShowDiscard(false)}
                        />

                        {/* Delete Confirmation */}
                        <ConfirmationModal
                            isModalShow={showDelete}
                            confirmationText={msgDeleteComment}
                            handleConfirmAction={() => isComment ? alert("Comment Delete") : alert("Reply Delete")}
                            handleCloseModal={() => setShowDelete(false)}
                        />

                        {showCreateReply && (
                            <RenderCreateReply
                                commentId={props.commentId}
                                mobileView={mobileView}
                            />
                        )}
                    </React.Fragment>
                );
            }}
        </Media>
    )
}

export default ResponseArea;