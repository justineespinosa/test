import React from 'react';
import {Container } from 'react-bootstrap';
import RenderCommentItem from './RenderCommentItem';
import './styles.scss';

const RenderComments = ({ postId, source, comments, userId }) => {
    if (comments) {
        return (
            <Container className="post-view-comment-area">
                {comments.map((item, i) => {
                    return (
                        <RenderCommentItem
                            {...item}
                            key={"comment" + i}
                            postId={postId}
                            source={source}
                            userId={userId}
                        />
                    );
                })}
            </Container>
        )
    } else {
        return '';
    }
}

export default RenderComments;