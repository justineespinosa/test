import React, {useState} from 'react';
import { Button } from 'react-bootstrap';
import { WRAP_TEXT_CHARS } from '../../../utils/constants';
import './styles.scss';

const PostContent = ({ title, postBody }) => {
    const [showFull, setShowFull] = useState(false);

    if (WRAP_TEXT_CHARS < postBody.length && !showFull) {
        let trimmedText = postBody.substring(0, WRAP_TEXT_CHARS);
        return (
            <span>
                <p className="post-title">{title}</p>
                {trimmedText + " . . ."}
                <Button className="post-body-button" variant="link" onClick={() => setShowFull(true)}>もっと見る</Button>
            </span>
        )
    } else {
        return <React.Fragment><p><b>{title}</b></p>{postBody}</React.Fragment>;
    }
}


export default PostContent;