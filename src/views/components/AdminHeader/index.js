import React from 'react';
import { Navbar, Dropdown } from 'react-bootstrap';
import ProfileIcon from '../../../includes/img/admin-menu/profile.svg';
import './styles.scss';

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
        href="_blank"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
        className="admin-header-dropdown"
    >
        <img alt="profile-icon" src={ProfileIcon} className="admin-header-name" />
        {"Handle Name"}
        <span className="arrow-down">&#x25bc;</span>
    </a>
));

const AdminHeader = () => {
    return (
        <Navbar className="admin-header">
            <Navbar.Brand style={{fontSize: 14}}>藤井フミヤオフィシャル comu comu</Navbar.Brand>
            <Dropdown>
                <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components" />

                <Dropdown.Menu>
                    <Dropdown.Item eventKey="1">Menu Option 1</Dropdown.Item>
                    <Dropdown.Item eventKey="2">Menu Option 2</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
        </Navbar>
    );
};

export default AdminHeader;