import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Route, /*Redirect*/} from 'react-router-dom';
import { authOperations } from '../containers/Authentication/state';

class PrivateUserRoute extends Component {
    render() {
        //========= DEV CHECKING ===========
        return (
            <div>
                <Route path={this.props.path} component={this.props.component} exact={this.props.exact} />
            </div>
        )

        //========= ACTUAL CHECKING ===========
        // return this.props.isAuthenticated && "ROLE_USER" === this.props.user.access ?
        //     <div>
        //         <Route path={this.props.path} component={this.props.component} exact={this.props.exact} />
        //     </div>
        //     : (
        //         //Redirect to App Root
        //         <Redirect to={{
        //             pathname: APP_ROOT_ROUTE,
        //             state: {
        //                 // from: this.props.location
        //             }
        //         }} />
        //     )
    }
}

const mapStateToProps = (state) => {
    return {
        isAuthenticated: state.auth.isAuthenticated,
        user: state.auth.credentials
    }
};

const mapDispatchToProps = (dispatch) => bindActionCreators(
    {
        logoutUser: authOperations.logoutUser
    },
    dispatch
);

export default connect(mapStateToProps, mapDispatchToProps)(PrivateUserRoute);