import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    fixedPCHeader:{
        display: 'flex',
        position: "fixed",
        zIndex: 9999999,
        width: '100%',
        justifyContent: 'space-between',
        alignItems: 'center',
        top: 0,
        backgroundColor: '#0A182C',
        height: 50
    },
    fixedMobileHeader:{
        position: "fixed",
        zIndex: 999,
        width: '100%',
        top: 0
    },
    title:{
        color: 'white', 
        fontSize: 18
    },
    
    mobileHeader:{
        backgroundColor: '#0A182C',
        justifyContent: 'center'
    },
    navigation:{
        flex: 1, 
        backgroundColor: "#fff",
        flexDirection: "row", 
        justifyContent: "space-around",
        paddingBottom: 19,
        marginTop: 0,
        borderBottomWidth: 2,
        borderBottomColor: '#F3f3f3'
    },
    icon:{
        width: 28, 
        height: 28
    }
})

export default styles;