import React, { useState } from 'react';
import { View, Text, Image } from 'react-native';
import { Container, Dropdown } from 'react-bootstrap';
import { Header, Icon, Button } from 'react-native-elements'
import { withRouter } from "react-router-dom";
import styles from './styles.native.js';
import { Link } from 'react-router-dom';
import { appendUserRoute } from '../../../utils/common.js';
import { isMobile } from "react-device-detect";
import { Media } from 'react-breakpoints'
import HamburgerMenu from '../HamburgerMenu';
import HamburgerIcon from '../../../includes/img/user-menu/burger-white.svg';

const navigation = [
    { icon: require('../../../includes/img/user-menu/newsfeed.svg'), navigate: "/newsfeed" },
    { icon: require('../../../includes/img/user-menu/mypage.svg'), navigate: "/mypage" },
    { icon: require('../../../includes/img/user-menu/chat.svg'), navigate: "/chat" },
    { icon: require('../../../includes/img/user-menu/search.svg'), navigate: "/search" },
    { icon: require('../../../includes/img/user-menu/notification.svg'), navigate: "/notifications" },
    { icon: require('../../../includes/img/user-menu/burger.svg'), navigate: "/menu" },
]

const CustomToggle = React.forwardRef(({ children, onClick }, ref) => (
    <a
        href="_blank"
        ref={ref}
        onClick={(e) => {
            e.preventDefault();
            onClick(e);
        }}
    >
        <img alt="hamburger-icon" src={HamburgerIcon} style={{ width: 24, height: 24 }} />
    </a>
));

const PCHeader = () => {
    const [search, setSearch] = useState("");

    return (
        <View style={styles.fixedPCHeader}>
            <Container className="flex-between-center" style={{height: 50}}>
                <Text style={styles.title}>{'comu comu'}</Text>

                <input
                    className="input-radius center-placeholder"
                    style={{ width: 250, height: 30, fontSize: 14 }}
                    type="search"
                    value={search}
                    placeholder={"検索する"}
                    onChange={(e) => setSearch(e.target.value)}
                />

                <Dropdown>
                    <Dropdown.Toggle as={CustomToggle} id="dropdown-custom-components" />
                    <HamburgerMenu />
                </Dropdown>
            </Container>
        </View>
    )
}

const MobileHeader = (props) => {
    let showBackButton = navigation.filter(item=> appendUserRoute(item.navigate) === props.location.pathname).length <= 0; 

    return (
        <View style={styles.fixedMobileHeader}>
            <Header
                placement="center"
                leftComponent={showBackButton && (
                    <Button
                        key={"back"}
                        type="clear"
                        icon={
                            <Icon
                                name="chevron-left"
                                size={30}
                                color="white"
                                type={"material"}
                            />
                        }
                        onPress={()=>props.history.goBack()}
                    />
                )}
                centerComponent={<Text style={styles.title}>{'comu comu'}</Text>}
                containerStyle={styles.mobileHeader}
            />

            <View style={styles.navigation}>
                {navigation.map((item, index) => {
                    let appendedRoute = appendUserRoute(item.navigate);
                    return (
                        <Link key={appendedRoute} href={appendedRoute} to={appendedRoute}>
                            <Button
                                key={index}
                                type="clear"
                                icon={
                                    <Image
                                        source={item.icon}
                                        style={styles.icon}
                                        resizeMode={"contain"}
                                    />
                                }
                                style={styles.icon}
                                onPress={() => { }}
                            />
                        </Link>
                    )
                })}
            </View>
        </View>
    )
}

const AppHeader = (props) => {
    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                if (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape || isMobile) {
                    return <MobileHeader {...props} />;
                } else {
                    return <PCHeader {...props} />
                }
            }}
        </Media>
    )
}

export default withRouter(AppHeader);


