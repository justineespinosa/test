import React from 'react';
import { Row, Col, Image, Container } from 'react-bootstrap';
import { TouchableOpacity } from 'react-native';
import { Media } from 'react-breakpoints'
import { isMobile } from "react-device-detect";
import styles from './styles.native.js';
import './styles.scss';

const maxPcContainerHeight = 750;
const maxMobileContainerHeight = 550;

const PhotoGrid = ({ source, onPressImage }) => {
    let count = source.length || 0;
    let gridComponent = "";

    const ImagePress = ({ src, style }) => {
        return (
            <TouchableOpacity activeOpacity={0.7} onPress={onPressImage} style={style}>
                <Image src={src} className="grid-image" />
            </TouchableOpacity>
        );
    }
    const OneImage = () => {
        return <ImagePress src={source[0]} style={styles.fullGrid} />
    }

    const TwoImage = () => {
        return (
            <span className="d-flex justify-content-between">
                {source.map((item, i) => <ImagePress key={"img" + i} src={item} style={styles.halfGridHorizontal} />)}
            </span>
        )
    }

    const ThreeImage = ({ maxContainerHeight, mobileView }) => {
        return (
            <React.Fragment>
                <div style={{ ...styles.halfGridVertical, paddingBottom: mobileView ? 3 : 5 }}><ImagePress src={source[0]} style={{ height: maxContainerHeight / 2 }} /></div>

                <span className="d-flex justify-content-between">
                    {source.map((item, i) => {
                        if (0 < i) {
                            return <ImagePress key={"img" + i} src={item} style={styles.halfGrid} />
                        }
                        return '';
                    })}
                </span>
            </React.Fragment>
        )
    }

    const FourImage = () => {
        return (
            source.map((item, i) => {
                let customStyle = {};
                if(2 > i){
                    customStyle = {...customStyle, paddingBottom: 4}
                }
                if(1 === ((i+1) % 2)){
                    customStyle = {...customStyle}
                }
                return (
                    <ImagePress 
                        key={"img" + i} 
                        src={item} 
                        style={{...styles.halfGridHorizontal, ...customStyle}} 
                    />
                )
            })
        )
    }

    const MultipleImage = ({ maxContainerHeight, mobileView }) => {
        let rightImages = [];
        let leftImages = [];
        source.map((item, i) => i < 2 ? leftImages.push(item) : (i < 5 && rightImages.push(item)));

        return (
            <React.Fragment>
                <Col className="grid-multi" style={{ maxHeight: maxContainerHeight }}>
                    {leftImages.map((item, i) => <ImagePress key={"left" + i} src={item} style={styles.halfGridVertical} />)}
                </Col>
                <Col className="grid-multi" style={{ maxHeight: maxContainerHeight }}>
                    {rightImages.map((item, i) => <ImagePress key={"right" + i} src={item} style={mobileView ? styles.trioGridVerticalMobile : styles.trioGridVertical} />)}
                </Col>

                {5 < source.length && (
                    <TouchableOpacity activeOpacity={0.7} onPress={onPressImage} style={mobileView ? styles.countOverlayMobile : styles.countOverlay}>
                        <p style={styles.count}>+{source.length - 5}</p>
                    </TouchableOpacity>
                )}
            </React.Fragment>
        )
    }



    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = breakpoints[currentBreakpoint] < breakpoints.mobileLandscape || isMobile;
                let maxContainerHeight = mobileView ? maxMobileContainerHeight : maxPcContainerHeight;

                switch (count) {
                    case 0: { gridComponent = ''; break; }
                    case 1: { gridComponent = <OneImage />; break; }
                    case 2: { gridComponent = <TwoImage />; break; }
                    case 3: { gridComponent = <ThreeImage maxContainerHeight={maxContainerHeight} mobileView={mobileView}/>; break; }
                    case 4: { gridComponent = <FourImage />; break; }
                    case 5:
                    default: { gridComponent = <MultipleImage maxContainerHeight={maxContainerHeight} mobileView={mobileView} />; break; }
                }

                return (

                    <Container style={{ padding: 0, margin: 0, maxHeight: maxContainerHeight, maxWidth: '100%' }}>
                        <Row className={4 === count ? "justify-content-between" : ""}  style={{ margin: 0, maxHeight: maxContainerHeight }}>
                            {gridComponent}
                        </Row>
                    </Container>
                )
            }}
        </Media>
    );
};



export default PhotoGrid;