import React from 'react';
import { Route } from 'react-router-dom';

const PublicRoute = (props) => {
    return(
        <Route 
            exact={props.exact}
            path={props.path} 
            component={props.component}
        />
    );
};

export default PublicRoute;