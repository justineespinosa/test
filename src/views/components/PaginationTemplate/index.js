import React from 'react';
import { Pagination, Button } from 'react-bootstrap';
import './styles.scss';

const PaginationTemplate = ({currentPage, totalPage, handlePageChange}) => {

    let firstPrev = 1 === currentPage || 0 === totalPage
    let nextLast = currentPage === totalPage || 0 === totalPage
    let items = [];

    for (let number = 1; number <= totalPage; number++) {
        items.push(
            <Pagination.Item key={number} active={number === currentPage} onClick={() => {handlePageChange(number)}}>
                {number}
            </Pagination.Item>
        );
    }

    return (
        
        <div>
            <Pagination style={{ float: 'right' }}>
                <Button className="pagination-caret" variant="link" disabled={firstPrev} onClick={() => { handlePageChange(1) }}>{"<<"}</Button>
                <Button className="pagination-caret" variant="link" disabled={firstPrev} onClick={() => { handlePageChange(currentPage-1) }}>{"<"}</Button>
                {items}
                <Button className="pagination-caret" variant="link" disabled={nextLast} onClick={() => { handlePageChange(currentPage+1) }}>{">"}</Button>
                <Button className="pagination-caret" variant="link" disabled={nextLast} onClick={() => { handlePageChange(totalPage) }}>{">>"}</Button>
            </Pagination>
        </div>
    )
}

export default PaginationTemplate;