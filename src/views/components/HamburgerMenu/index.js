import React from 'react';
import { isMobile } from "react-device-detect";
import { Media } from 'react-breakpoints'
import { Dropdown } from 'react-bootstrap';
import { MenuItem } from '../MenuItem';

export const navList = (mobileView) => {
    let appendName = mobileView ? "" : "-dark";
    return [
        { navigate: "/mypage", label: 'Handle Name', icon: require(`../../../includes/img/user-menu/profile${appendName}.svg`) },
        { navigate: "/livestream", label: 'ライブ動画配信', icon: require(`../../../includes/img/user-menu/livestream${appendName}.svg`) },
        { navigate: "/newsfeed", label: '最新の更新', icon: require(`../../../includes/img/user-menu/newsfeed${appendName}.svg`) },
        { navigate: "/chat", label: 'メッセージ', icon: require(`../../../includes/img/user-menu/chat${appendName}.svg`) },
        { navigate: "/friends", label: '友達リスト', icon: require(`../../../includes/img/user-menu/friends${appendName}.svg`) },
        { navigate: "/blocked", label: 'アクセスブロック', icon: require(`../../../includes/img/user-menu/block${appendName}.svg`) },
        { navigate: "/help", label: 'ヘルプ', icon: require(`../../../includes/img/user-menu/help${appendName}.svg`) },
        { navigate: "/contact-us", label: 'お問い合わせ', icon: require(`../../../includes/img/user-menu/contact${appendName}.svg`) },
        { navigate: "/communities", label: '過去のオフィシャルコミュニティ', icon: require(`../../../includes/img/user-menu/community${appendName}.svg`) },
        { navigate: "/privacy-policy", label: 'プライバシーポリシー', icon: require(`../../../includes/img/user-menu/document${appendName}.svg`) },
        { navigate: "/terms-of-service", label: '利用規約', icon: require(`../../../includes/img/user-menu/document${appendName}.svg`) },
        { navigate: "/logout", label: 'ログアウト', icon: require(`../../../includes/img/user-menu/logout${appendName}.svg`) },
        { navigate: "/twitter", label: 'Twitter', icon: require(`../../../includes/img/user-menu/twitter${appendName}.svg`), mobileOnly: true },
        { navigate: "/instagram", label: 'Instagram', icon: require(`../../../includes/img/user-menu/instagram${appendName}.svg`), mobileOnly: true },
        { navigate: "/facebook", label: 'Facebook', icon: require(`../../../includes/img/user-menu/facebook${appendName}.svg`), mobileOnly: true },
    ];
}

const HamburgerMenu = () => {
    return (
        <Media>
            {({ breakpoints, currentBreakpoint }) => {
                let mobileView = (breakpoints[currentBreakpoint] < breakpoints.mobileLandscape) || isMobile;
                if (mobileView) {
                    return (
                        <div style={{backgroundColor: "#fff"}}>
                            {
                                navList(mobileView).map((item, i) => {
                                    return (
                                        <MenuItem 
                                            key={i} 
                                            item={item}
                                            leftAvatar={{ rounded: false, source: item.icon, size: 32 }}
                                            titleStyle={{ fontWeight: '500', marginLeft: 16 }}
                                            bottomDivider={true}
                                        />
                                    )
                                })
                            }
                        </div>
                    );
                } else {
                    //PC Hamburger Menu
                    return (
                        <Dropdown.Menu alignRight style={{marginTop: 18, width: 300}}>
                            {
                                navList(mobileView).map((item, i) => {
                                    return <MenuItem key={i} item={item}/>
                                })
                            }
                        </Dropdown.Menu>
                    );
                }
            }}
        </Media>
    )
}
export default HamburgerMenu;


