import React from 'react';
import { View } from 'react-native';
import { ListItem } from 'react-native-elements';
import { Link } from 'react-router-dom';
import { appendUserRoute } from '../../../utils/common';

export const MenuItem = (props) => {
    let { item, action } = props;
    let appendedRoute = action ? "" : appendUserRoute(item.navigate);

    if (item.mobileOnly) {
        return "";
    }

    const renderItem = () => {
        return (
            <ListItem
                containerStyle={props.containerStyle || { paddingVertical: 10, backgroundColor: 'none' }}
                leftElement={<img src={item.icon} alt="menu-icon" style={{ width: 28, objectFit: 'contain' }} />}
                title={item.label}
                titleStyle={props.titleStyle || { fontWeight: '500', marginLeft: 16, fontSize: 14 }}
                bottomDivider={props.bottomDivider}
                onPress={(props.onPress)}
            />
        )
    }

    return (
        action ? (
            <View>
                {renderItem()}
            </View>
        ) : (
                <Link key={item.navigate} href={appendedRoute} to={appendedRoute} style={{ textDecoration: 'none' }}>
                    {renderItem()}
                </Link>
            )

    );
}