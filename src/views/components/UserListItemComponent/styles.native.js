import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  listContainer: {
    height: 650
  },
  title: {
    marginLeft: 20
  },
  emptyList: {
    textAlign: "center",
    marginLeft: 20
  },
  avatar: {
    width: 70,
    height: 70
  }
});

export default styles;
