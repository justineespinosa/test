import React from "react";
import styles from "./styles.native.js";
import { FlatList } from "react-native";
import { ListItem } from "react-native-elements";

const EmptyComponent = () => {
  return (
    <ListItem title="結果が見つかりません。" titleStyle={styles.emptyList} />
  );
};

const renderItem = (item, props) => {
  return (
    <ListItem
      title={item.handleName}
      titleStyle={styles.title}
      leftAvatar={{
        size: "xlarge",
        source: { uri: item.url },
        containerStyle: styles.avatar
      }}
      onPress={() => props.handleSelectItem(item)}
    />
  );
};

const UserListItemComponent = props => {
  if (!props.isCommunity) {
    return (
      <FlatList
        style={styles.listContainer}
        showsVerticalScrollIndicator={false}
        data={props.list}
        keyExtractor={item => item.userId.toString()}
        renderItem={({ item }) => renderItem(item, props)}
        extraData={props.list}
        ListEmptyComponent={EmptyComponent}
        onScroll={({ nativeEvent }) => {
          if (props.isAtBottom(nativeEvent) && !props.scrollEnd) {
            props.handlePagination();
          }
        }}
        bounces={false}
        onEndReached={() => {
          props.onScrollEnd();
        }}
      />
    );
  } else {
    //Render Community List
  }
};

export default UserListItemComponent;
