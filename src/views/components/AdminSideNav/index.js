import React from 'react';
import { Link } from 'react-router-dom';
import './styles.scss';
import { appendAdminRoute } from '../../../utils/common';
import UserIcon from '../../../includes/img/admin-menu/user.svg';
import NoticeIcon from '../../../includes/img/admin-menu/notification.svg';
import MessageIcon from '../../../includes/img/admin-menu/message.svg';
import MaintenanceIcon from '../../../includes/img/admin-menu/maintenance.svg';

const navigation = [
    { navigate: "/user-search", label: 'ユーザ検索', icon: UserIcon },
    { navigate: "/admin-notice", label: 'お知らせ', icon: NoticeIcon },
    { navigate: "/random-message", label: 'フミヤさんからのランダムメッセージ', icon: MessageIcon },
    { navigate: "/maintenance", label: 'System Maintenance Setting', icon: MaintenanceIcon }
]

const AdminSideNav = () => {
    return (
        <div className="admin-sidenav">
            <div>{"comu comu"}</div>
            <div>{"管理画面"}</div>
            <div className="admin-sidenav-menu">
                {navigation.map((item, i) =>{
                    let appendedRoute = appendAdminRoute(item.navigate);
                    return(
                        <Link key={item.navigate} href={appendedRoute} to={appendedRoute}>
                            <span key={"sidenav" + i} style={{display:"flex", alignItems: 'flex-start'}}>
                                <img alt="sidenav-icon" src={item.icon} className="admin-sidenav-icon"/>
                                <span>{item.label}</span>
                            </span>
                        </Link>
                    )
                })}
            </div>
        </div>
    );
};

export default AdminSideNav;