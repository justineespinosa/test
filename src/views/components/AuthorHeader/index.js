import React from 'react';
import { Image } from 'react-bootstrap';
import { appendUserRoute } from '../../../utils/common';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';
import DefaultIcon from '../../../includes/img/placeholders/user.svg';
import moment from 'moment';

import './styles.scss';
import { DATETIME_FORMAT } from '../../../utils/constants';

const HandleName = (props) => {
    let { appendedRoute, isUserDeleted, handleName } = props;
    if (isUserDeleted) {
        return (
            <span className="author-name">{"退会ユーザ"}</span>
        )
    } else {
        return (
            <Link href={appendedRoute} to={appendedRoute}>
                <span className="author-name">{handleName}</span>
            </Link>
        )
    }
}
const AuthorHeader = (props) => {
    //Redux store data
    const loggedUserId = useSelector(state => state.auth.credentials.userId);

    //User Route
    let appendedRoute = appendUserRoute(`/user/${props.userId}`);
    if (loggedUserId === props.userId) {
        appendedRoute = appendUserRoute("/myPage");
    }

    return (
        <div className="author-container">
            <React.Fragment>
                <Link href={appendedRoute} to={appendedRoute}>
                    <Image src={props.profileImageUrl || DefaultIcon} style={{ width: 36, height: 36, objectFit: 'cover' }} roundedCircle />
                </Link>
                <div className="d-flex flex-column ml-2">
                    <HandleName appendedRoute={appendedRoute} isUserDeleted={props.isUserDeleted} handleName={props.handleName}/>
                    <span className="author-datetime">
                        {moment(new Date(props.createDt)).format(DATETIME_FORMAT)}
                        {moment(new Date(props.updateDt)).isValid() && " | 編集済"}
                    </span>
                </div>
            </React.Fragment>
        </div>
    );


};

export default AuthorHeader;