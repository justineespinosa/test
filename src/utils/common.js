import { APP_ROOT_ROUTE } from "../views/containers/UserMain/routes";
import { ADMIN_ROOT_ROUTE } from "../views/containers/AdminMain/routes";
import { FUMIYA_USER_ID } from "../config/settings";
import ApiService from "./apiService";

export const appendUserRoute = (path) =>{
    return APP_ROOT_ROUTE + path;
}

export const appendAdminRoute = (path) =>{
    return ADMIN_ROOT_ROUTE + path;
}

export const isStringNullEmptyOrUndefined = (value) =>{
    return undefined === value || '' === value.toString().trim() || null === value.toString().trim();
}

export const isNullOrUndefined = (value) =>{
    return undefined === value || null === value;
}

export const sortDateByKey = (list, key, order) =>{
    switch(order){
        case 'desc': return list.sort((a, b) => new Date(b[key]) - new Date(a[key]));
        case 'asc':
        default: return list.sort((a, b) => new Date(b[key]) - new Date(a[key]));
    }
}

export const emptyTextValue = (value) => {
    if('string' === typeof value){
        return 0 >= value.trim().length;
    }else{
        return false;
    }
}

export const isFumiyaUserCheck = (userId) => {
    return FUMIYA_USER_ID === userId;
}

export const uploadToS3 = (imageFile, preSignedURL) =>{
    return ApiService.post(preSignedURL, imageFile)
    .then(response => {
       return response;
    })
    .catch(error => {
      console.error(error);
    });
};


