/*Define all constants*/
export const DATE_FORMAT = "YYYY/MM/DD";
export const DATETIME_FORMAT = "YYYY/MM/DD HH:MM"
export const API_SUCCESS_STATUS = "Success";
export const API_ERROR_STATUS = "Error";
export const EMPTY_STRING = ""
export const ON_MESSAGE = "message"
export const ON_IMAGE_UPLOAD = "imageUpload"
export const ON_MESSAGE_DELETE = "messageDelete"
export const ENTER_KEY = "Enter"
export const SEARCH_KEY = "search"
export const ACCEPTED_IMAGE_TYPES = ['image/jpg', 'image/jpeg', 'image/png'];

/*Error Messages*/
export const REQUIRED_FIELD = "テキストまたは画像入力してください。"
export const INVALID_IMAGE = "画像利用出来ない文字が含まれています。"
export const SYSTEM_ERROR = "システムエラー。"

/*Define all limits*/
export const WRAP_TEXT_CHARS = 250;
export const POST_MAX_CHAR = 50000;
export const COMMENTS_MAX_CHAR = 1000;
export const REPLIES_MAX_CHAR = 1000;
export const CHAT_MAX_CHAR = 1000;
export const IMAGE_MAX_UPLOAD = 10;
export const THREAD_MAX_CHAR = 50;

export const ATTACHMENT_TYPE = {
    none: 0,
    image: 1,
    video: 2
}

export const POST_TYPES = {
    normal: 0,
    livestream: 1,
    album: 2
}

export const UPLOAD_STATUS = {
    uploading: 0,
    success: 1,
    failed: 2
}

export const MODE = {
    add: "add",
    edit: "edit",
    confirm: "confirm"
}

export const SOURCE = {
    new: 0,
    oneGeneration: 1,
    twoGeneration: 2
}

export const CHAT_TYPE = {
    text: 0,
    image: 1
}


export const WEBSOCKET_ACTIONS = {
    onCreateConnection: "createConnection",
    onMessage: "onMessage",
    onMessageDelete: "onMessageDelete",
    updateSeenStatus: "updateSeenStatus"
}

export const WEBSOCKET_MESSAGES = {
    onMessage: "message",
    onImageUpload: "imageUpload",
    onImageRender: "imageRender",
    onMessageDelete: "messageDelete",
  };
